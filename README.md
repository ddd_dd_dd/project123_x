db-migration-service
Правила нумерации и тегирования миграций
скрипты создаём с версией, поднимая только вторую цифру, третья цифра всегда 0;
теги для сборки ставим по последней версии скрипта.
Если такой тег уже выкатывался, то поднимается последняя цифра
(например для добавления повторяющегося скрипта без версии);
сервис миграции гарантированно сначала выполняет версионные скрипты(V...), затем повторяющиеся(R...) - инфа от Сергея Будрика
Например:
когда был добавлен скрипт 1.276.0 - тег dev-1.276.0
потом когда добавляем скрипт repeatable_scripts/R__tdm_upsertSql.sql - тег dev-1.276.1

Поддерживаемые скрипты.
Версионные скрипты
V1.1.1__2021-04-27_DRS2021_description_script.sql

Повторяющиеся скрипты
R__schemaDataBase_nameFunctionDataBase.sql

История:
V4.12.0__2024-03-05_task_4.sql

V4.11.0__2024-03-05_task_3.sql

V4.10.0__2024-03-05_task_2.sql

V4.9.0__2024-03-05_task_1.sql

V4.8.0__2024-03-05_CZS2022-3767_insert_data_asuds_nsstani_nsi_gid_stan_subst.sql

V4.7.0__2024-03-04_CZS2022-3726_new_tables_nsi_station_type_object.sql

V4.6.0__2024-03-04_CZS2022-3765_correction_tdm_tm_otmena_top_tdm_tm_otmena_tov.sql

V4.5.0__2024-03-04_CZS2022-3758_add_column_tdm_tm_vag_op_tdm_tm_vag_all.sql

V4.4.0__2024-03-04_CZS2022-3752_update_nsi_operation_map_asustnp.sql

R__pnxSidebarAsuds_getWaggonList.sql

V4.3.0__2024-03-01_CZS2022-3735_gate_tdm_name_ob_map_add_column.sql

V4.2.0__2024-03-01_CZS2022-3741_replace_all_config_stations.sql

V4.1.0__2024-03-01_CZS2022-3760_replace_filter_kategory_pikop.sql

R__gateFix_getTrainVags.sql R__gateFix_getTrainVags2.sql

V3.108.0__2024-02-21_CZS2022-3669_add_columns_tdm__tm_object_op.sql

V3.107.0__2024-02-21_CZS2022-3679_add_columns_gate_tdm__name_ob_map.sql

V3.106.0__2024-02-19_CZS2022-3641_update_approach_tables.sql

V3.105.0__2024-02-19_CZS2022-3509_update_ns016.sql

V3.104.0__2024-02-19_CZS2022-3594_spec_put_updating.sql

V3.103.0__2024-02-14_CZS2022-3643_correction_cbdgr.sql

V3.102.0__2024-02-14_CZS2022-3630_wps_stan_view_config.sql

V3.101.0__2024-02-14_CZS2022-3627_update_nsi_cnsi_nsstanid.sql

V3.100.0__2024-02-14_CZS2022-3622_update_api__timer_setting.sql

V3.99.0__2024-02-09_CZS2022-3596_update_api_sys_options.sql

V3.98.0__2024-02-12_CZS2022-3604_correction_cbdg.sql

V3.97.0__2024-02-09_CZS2022-3596_update_api_sys_options.sql

V3.96.0__2024-02-09_CZS2022-3564_correction_cbdg.sql

V3.95.0__2024-02-09_CZS2022-3596_insert_into_api_sys_options.sql

V3.94.1__2024-02-08_CZS2022-3588_add_api_system.sql

V3.94.0__2024-02-08_CZS2022-3588_correction_cbdgr.sql

V3.93.0__2024-02-07_CZS2022-3575_correction_cbdgr.sql

V3.92.3__2024-02-07_CZS2022-3564_correction_cbdgr.sql

V3.92.2__2024-02-07_CZS2022-3564_correction_cbdgr.sql

V3.92.1__2024-02-06_CZS2022-3564_correction_cbdgr.sql

V3.92.0__2024-02-06_CZS2022-3564_correction_cbdgr.sql

V3.91.0__2024-02-02_CZS2022-3424_new_tables_in_nsi.sql

R__gateCore_getIdСontainerPlatform.sql

V3.90.0__2024-02-01_CZS2022-3463_new_schema__cbdgr_timetable.sql

V3.89.0__2024-01-31_CZS2022-3506_add_column.sql

V3.88.0__2024-01-31_CZS2022-3509_insert_nsi_ns015_ns016_ns019.sql

V3.87.0__2024-01-31_CZS2022-3485_add_api__timer_setting.sql

V3.86.0__2024-01-31_CZS2022-3437_new_tables_operation_map.sql

V3.85.0__2024-01-30_CZS2022-3501_update_wps__stan_view_config.sql

V3.84.0__2024-01-30_CZS2022-3497_update_tp_operation.sql

V3.83.0__2024-01-29_CZS2022-3479_insert_into_api_systems.sql

V3.82.0__2024-01-26_CZS2022-3449_new_schema_approach.sql

V3.81.0__2024-01-23_CZS2022-3398_insert_into_api_sys_options.sql

V3.80.0__2024-01-16_CZS2022-3373_update_webplsql_rest_service_routes.sql

R__wpsJournalSmAdv_getNaznInNormV5.sql

V3.79.0__2024-01-12_CZS2022-3354_add_column_nsi__ns_kateg_poezd_mdl.sql

V3.78.0__2024-01-12_CZS2022-3355_add_columns_mdl__ekasut_order.sql

V3.77.0__2024-01-10_CZS2022-3349_add_column_sl_mark.sql

V3.76.0__2024-01-11_CZS2022-3342_update_stan_view_config.sql

R__wpsJournalSmAdv_getNaznInNormV5.sql фикс

V3.75.0__2023-12-21_CZS2022-3251_new_columns_mdl__manual_correct.sql

V3.74.0__2023-12-21_CZS2022-3256_new_table_nsi_plan_form_asov.sql

V3.73.0__2023-12-21_CZS2022-3258_new_column_nsi_stan.sql

R__mdlCore2_getModelCarsTrain_new.sql R__mdlCore2_getModelCarsTrain_connection.sql V3.72.0__2023-12-20_add_attribute_type.sql

V3.71.0__2023-12-20_CZS2022-3235_new_column_nsi__ns_vid_poezd.sql

R__mdlCore2_getModelTrainPriorities.sql R__gateCoreCar_insUpdTmVagGroup.sql

V3.70.0__2023-12-19_CZS2022-3193_add_type_mdl_core2_ttrainpriorityinfo.sql

R__mdlCore2_getModelTrainPriorities.sql

V3.69.0__2023-12-18_CZS2022-3200_new_column_mdl_manual_correct.sql

R__gateCoreCar_insUpdTmVagGroup.sql R__gateCoreCar_processVagOperation_v2.sql R__gateCoreCar_processVagOperationJson_v2.sql

V3.68.0__2023-12-13_CZS2022-3152_comment_tables_manual_correct.sql

V3.67.0__2023-12-09_CZS2022-3105_new_TP_spec19.sql

V3.66.0__2023-12-08_CZS2022-3086_new_column_mdl_tm_plan_vag_op.sql

V3.65.0__2023-12-08_CZS2022-3064_new_column_nsi_put.sql

R__wpsJournalSmAdv_getNaznInNormV5.sql

V3.64.0__2023-12-07_CZS2022-3047_insert_nsi_put_direction.sql

V3.63.0__2023-12-07_CZS2022-3059_insert_mng_sys_sys_options.sql

V3.62.0__2023-12-06_CZS2022-2939_table_to_view_nsi_put_enmity_oversized.sql

R__gateTools_getIdPoezdByExt_v2.sql R__gateTools_getIdPoezdByExt.sql

V3.61.0__2023-12-04_CZS2022-2968_table_to_view_nsi_nsi.plan_form_and_spec_put.sql

R__gateCoreCar_insUpdTmVagGroup.sql

R__gateCoreCar_insUpdTmVagGroup.sql

R__gateCoreCar_insUpdTmVagGroup.sql V3.60.0__2023-12-03_CZS2022-2991_add_api_options.sql

R__gateCoreCar_insUpdTmVagGroup.sql R__gateCoreCar_processVagOperation_v2.sql R__gateCoreCar_processVagOperationJson_v2.sql

R__gateCoreCar_insUpdTmVagGroup.sql R__gateCoreCar_cutOutCarFromOldTrain.sql

V3.59.0__2023-12-01_CZS2022-2933_alter_nsi_put_direction.sql

V3.58.0__2023-11-29_CZS2022-2968_add_index_nsi_plan_form.sql

V3.57.0__2023-11-29_CZS2022-2968_table_to_view_nsi_nsi.plan_form_and_spec_put.sql

V3.56.0__2023-11-29_add_mdl_props.sql

V3.55.1__2023-11-30_CZS2022-2977_add_attribute_type.sql V3.55.0__2023-11-29_CZS2022-2977_add_attribute_type.sql R__mdlCore2_getModelCarsTrain_connection.sql R__mdlCore2_getModelCarsTrain_new.sql

R__gateCoreCar_insUpdTmVagGroup.sql R__gateCoreCar_processVagOperation_v2.sql R__gateCoreCar_processVagOperationJson_v2.sql

R__gateCoreCar_insUpdTmVag.sql R__gateCoreCar_insUpdTmVagGroup.sql R__gateCoreCar_insUpdTmVag_v2.sql R__gateCoreCar_processVagOperationJson_v2.sql R__gateCoreCar_processVagOperation_v2.sql R__gateCoreCar_queryTmVagAllWithPrevVagOrder.sql R__gateCoreCar_setVagKategMestSost.sql R__gateFix_fixTmOp.sql R__gateFix_getTrainVags.sql R__gateFix_getTrainVags2.sql V3.53.0__2023-11-29_DRS2022-1771_create_table_tm_vag_all.sql V3.54.0__2023-11-29_DRS2022-1772_create_view_tm_vag.sql

V3.52.0__2023-11-28_CZS2022-2699_sl_mark.sql

R__nsi_getPutIdByEsrAndNomParkAndNomPut.sql V3.51.0__2023-11-27_CZS2022-2933_alter_nsi_put.sql

R__mrmZakr_getIdBash.sql

V3.50.0__2023-11-21_CZS2022-2845_add_attribute_type.sql

correct V3.42.0__2023-11-15_CZS2022-2500_VP_02361941.sql

R__mdlCore2_getModelCarsTrain_new.sql

R__wpsSchemeProstPlan_getprostdetVagTdm.sql

V3.49.0__2023-11-19_CZS2022-2844_new_columns_mdl__tm_plan_op.sql

V3.48.0__2023-11-17_CZS2022-2736_new_tables_manual_correct.sql

V3.47.0__2023-11-17_CZS2022-2805_insert_nsi__ns019_asp2.sql

V3.46.0__2023-11-16_CZS2022-2766_insert_mng_sys_sys_options.sql

V3.45.0__2023-11-16_CZS2022-2802_insert_webplsql_rest_service_routes.sql

V3.44.0__2023-11-16_CZS2022-2765_insert_nsi_splmt.sql

V3.43.0__2023-11-16_CZS2022-2764_insert_mng_sys_pnx_permissions.sql

R__pnxTechprocess_migrationWorkTwoSignalers.sql

V3.42.0__2023-11-15_CZS2022-2500_VP_02361941.sql

R__pnxArmTreeView_detDepTrains.sql

V3.41.0__2023-11-09_CZS2022-2699_new_table_SL.sql

V3.40.0__2023-11-09_CZS2022-2702_update_nsi__cnsi_nsstanid.sql

V3.39.0__2023-11-06_CZS2022-2675_add_tech_process.sql

R__mdlCore2_getModelCarsTrain_new.sql R__mdlCore2_getModelCarsTrain_connection.sql V3.38.0__2023-11-06_alter_type_tcarinfo_ext2.sql

R__pnxArmTreeView_detDepTrains.sql

V3.37.0__2023-11-31_CZS2022-2627_update_tp_shunting_work.sql

R__mdlLoc_getmodelshuntinglocomotives.sql R__mdlLoc_getmodellocomotives.sql

V3.36.0__2023-10-29_CZS2022-2484_tehprocess_51_52.sql

V3.35.0__2023-10-29_add_atribut_loc.sql

V3.34.0__2023-10-29_CZS2022-2487_create_table_nsi__drs_station.sql

V3.33.0__2023-10-22_CZS2022-2194_update_tp_operation.sql

V3.32.0__2023-10-22_CZS2022-1867_alter_type_tcarinfo_ext2.sql R__mdlCore2_getModelCarsTrain_new.sql R__mdlCore2_getModelCarsTrain_connection.sql

V3.31.0__2023-10-21_CZS2022-2194_update_tp_operation.sql

V3.30.0__2023-10-19_CZS2022-1764_revert_view_tdm_vag.sql

V3.29.0__2023-10-19_CZS2022-2289_script_special_TP.sql

R__getCoreTrain_setTrainOperation.sql

V3.28.0__2023-10-10_CZS2022-2194_add_column_tp_operation.sql

R__gateCore_tryGetTrain.sql R__gateTools_getIdPoezdByExt.sql

V3.27.0__2023-10-06_CZS2022-2031_asuds__mrm_spis_nar.sql

V3.26.0__2023-10-06_CZS2022-2106_tp_n30_update.sql

V3.25.0__2023-10-05_CZS2022-2000_newTP_TPoperations_ns015.sql

V3.25.1__2023-10-05_CZS2022-2033_new_tp_n31_32.sql

R__mdlCore2_getModelCarsTrain_new.sql R__mdlCore2_getModelCarsTrain_connection.sql

V3.23.0__2023-10-03_CZS2022-1791_update_cnsi_nsstanid.sql

V3.22.0__2023-10-02_CZS2022-1764_new_view_tdm_vag.sql

V3.21.0__2023-10-02_CZS2022-1705_add_column_previous_nomvag.sql

V3.20.0__2023-09-20_CZS2022-1708_add_api_options.sql

V3.19.0__2023-09-29_CZS2022-1999_new_columnss_mdl__ekasut_order.sql

V3.18.0__2023-09-27_CZS2022-1968_some_manipulations_with_tables_and_data.sql

R__pnxArmTreeView_getPodhodRaw_v2.sql

V3.17.0__2023-09-26_CZS2022-1964_add_columns.sql

V3.16.0__2023-09-26_CZS2022-1963_create_table_nsi_tpws_group.sql

R__wpsJournalMd5_saveVVData.sql

V3.15.0__2023-09-22_CZS2022-1953_drop_tryCreateTrain.sql R__gateCore_insertTmObjectMapEx.sql R__gateCore_tryCreateTrain.sql R__gateCore_tryCreateTrain_v2.sql R__gateCore_tryGetTrain.sql

R__gateTools_getIdPoezdByExt.sql R__gateCore_tryGetTrainOldNew.sql R__gateCore_tryGetTrain.sql R__getCore_tryFindTrainByExt.sql

V3.14.0__2023-09-18_CZS2022-1825_add_columns_gate_tdm_tm_object_map_ex.sql

V3.13.0__2023-09-15_CZS2022-1848_new_api__systems.sql

R__techPoly_kategMestSost.sql

V3.12.0__2023-09-14_CZS2022-1851_add_columns.sql

R__wpsJournal_getLocPodh.sql

V3.11.0__2023-09-08_CZS2022-1773_new_table.sql

R__pnxArmTreeView_detDepCars.sql R__pnxArmTreeView_detArrCars

V3.10.0__2023-09-01_CZS2022-1728_add_column_fix_side_code.sql

V3.9.0__2023-09-01_CZS2022-1724_add_status.sql

V3.8.0__2023-08-31_CZS2022-1696_change_comments.sql

V3.7.0__2023-08-31_CZS2022-1718_add_api_options.sql

V3.6.0__2023-08-31_CZS2022-1642_add_column_tech_oper_id_to_mdl_tm_plan_op.sql

V3.5.0__2023-08-29_CZS2022-1691_create_table_nsi_shunting_work_mark.sql

V3.4.0__2023-08-29_CZS2022-1642_add_column_shunting_work_to_nsi_operation_nsi_tpws_oper.sql

R__mdlCore2_getmodelcontrolactions.sql

V3.3.0__2023-08-29_CZS2022-250_drop_func_pnx_sidebar_asuds_getwaggonlist.sql

V3.2.0__2023-08-29_CZS2022-1698_update_nsi_cnsi_nsstanid.sql

R__wZakrgraf_jurPrikazSaveJson.sql

V2.100.0__2023-08-25_CZS2022-1663_change_column.sql

V2.99.0__2023-08-24_CZS2022-1431_insert_mng_sys_pnx_permissions.sql

V2.98.0__2023-08-24_CZS2022-1666_update_stan_view_config.sql

R__techPoly_kategMestSost.sql

R__getCoreTrain_insertUpdateTmObjects.sql

R__gateCoreTrain_insertTrainOperation.sql R__getCoreTrain_processTrainOperation.sql R__getCoreTrain_processTrainOperationGid.sql

R__pnxArmTreeView_detDepCars.sql

V2.97.0__2023-08-17_CZS2022-1332_alter_nsi_progressive_number_train_angular_nsi_train_number_angular.sql

V2.96.0__2023-08-16_CZS2022-1385_ns_kateg_poezd_mdl_rename_column_comment.sql

V2.95.0__2023-08-16_CZS2022-1412_add_column_nsi_spec_put.sql

remove V2.92.0__2023-08-09_CZS2022-1378_fix_side_move.sql

V2.94.0__2023-08-10_CZS2022-1508_add_api_options.sql

V2.93.0__2023-08-09_CZS2022-1534_index_timetable_gruzp.sql

R__wZakrgraf_getPoezdOpTime.sql

V2.93.0__2023-08-09_CZS2022-1534_index_timetable_gruzp.sql

V2.92.0__2023-08-09_CZS2022-1378_fix_side_move.sql

V2.91.0__2023-08-07_CZS2022-1476_add_column.sql

R__wpsJournalMd5_saveVVData.sql

V2.90.0__2023-08-03_CZS2022-1496_mrm_fn_ispol.sql

R__wpsJournal_sendZakazAsut.sql

R__wpsJournalMd5_saveVVData.sql

R__wpsJournal_sendZakazAsut.sql V2.89.0__2023-08-02_CZS2022-1309_drop_func_sendZakazAsut.sql

R__wpsJournal_getLocStan.sql R__wpsJournal_getLocPodh.sql

V2.88.0__2023-08-01_CZS2022-1477_alter_type_mrm_spis_nar.sql

V2.87.0__2023-08-01_CZS2022-1385_add_report_categ.sql

R__pnxArmTreeView_detDepCars.sql

V2.86.0__2023-07-27_CZS2022-1332_create_and_update_tables_for_train_angular.sql

R__wpsJournalMd5_saveVVData.sql

R__pnxArmTreeView_detArrCars.sql

V2.85.0__2023-07-27_CZS2022-1436_add_table.sql

V2.84.0__2023-07-27_CZS2022-1326_update_nsi_tpws_protz.sql

V2.83.0__2023-07-27_CZS2022-1346_add_attr_tparktotaldetailtrain.sql R__pnxArmTreeView_detArrTrains.sql

R__mdlCore2_getModelTrains.sql

V2.82.0__2023-07-26_CZS2022-1370_add_column.sql

V2.81.0__2023-07-26_CZS2022-1379_add_table.sql

R__nsi_technologicalProcessEditorProcessView.sql

V2.80.1__2023-07-25_CZS2022-1378_asuds_kateg.sql V2.80.0__2023-07-24_CZS2022-1378_train_category.sql

V2.79.0__2023-07-12_CZS2022-1297_add_column_into_mdl_ekasut_order.sql

V2.78.0__2023-07-20_CZS2022-1390_update_nsi_cnsi_nsstanid.sql

V2.77.0__2023-07-18_CZS2022-1314_new_mrm_fn_task_type.sql

V2.74.0__2023-07-17_CZS2022-1357_update_nsi_cnsi_nsstanid.sql

V2.73.0__2023-07-15_CZS2022-1359_add_table.sql

V2.72.0__2023-07-14_CZS2022-1358_correct_uols.sql V2.71.0__2023-07-14_resize_column_type.sql

R__wpsJournal_getLocPodh.sql && R__wpsJournal_getLocStan.sql R__wpsJournal_sendZakazAsut.sql V2.70.0__2023-07-13_CZS2022-1309_drop_func_sendZakazAsut.sql

R__gateCoreCar_processVagOperation.sql

R__techPoly_kategOp4100.sql

V2.69.0__2023-07-12_CZS2022-1210_renew_asut_nsi_00.sql

V2.68.0__2023-07-12_CZS2022-1297_add_column_mdl_ekasut_order.sql

R__mrmZakr_getPrchetZakr.sql

R__wZakrgraf_saveMrmFnTask.sql restore

R__mrmZakr_zakrSave.sql R__mrmZakr_getPrchetZakr.sql V2.67.0__2023-07-11_CZS2022-1154_drop_func_ZAKR_SAVE.sql

V2.66.0__2023-07-11_CZS2022-1315_alter_sequence_uols.sql

V2.65.0__2023-07-11_CZS2022-1288_drop_w_zakrgraf_get_poezd_op_time.sql

V2.64.0__2023-07-11_CZS2022-1312_correct_cnsi_nsstanid.sql

R__wZakrgraf_saveMrmFnTask.sql revert

R__mrmZakr_zakrSave.sql R__mrmZakr_getPrchetZakr.sql V2.63.0__2023-07-11_CZS2022-1154_revert.sql

R__wZakrgraf_saveMrmFnTask.sql

R__wpsJournalMd5_saveVVData.sql

V2.62.0__2023-07-10_CZS2022-1149_add_column_index.sql

R__mrmZakr_zakrSave.sql R__mrmZakr_getPrchetZakr.sql V2.61.0__2023-07-10_CZS2022-1154_drop_func_ZAKR_SAVE.sql

R__wpsJournalMd5_saveVVData.sql

R__mdlLoc_getmodellocomotives.sql

R__wpsJournal_setReasonChangeLocomotive.sql R__wpsJournal_setReasonChangeLocomotiveBrig.sql

V2.60.0__2023-07-06_CZS2022-1264_add_row_opcode_matching.sql

R__wpsJournalMd5_saveVVData.sql

V2.59.0__2023-07-05_CZS2022-1230_drop_SetReasonChangeLocomotive.sql

R__wZakrgraf_saveCheck.sql R__wPrn_coalesce.sql

R__pkgRepPlanFact_pipedPlanTrains.sql R__pkgRepPlanFact_pipedPlanTrainsRow.sql

V2.58.0__2023-07-04_CZS2022-1215_add_columns_to_mrm_fn_task.sql

V2.57.0__2023-07-03_CZS2022-1226_update_nsi_cnsi_nsstanid.sql

V2.56.0__2023-07-03_CZS2022-1247_drop_function.sql R__wpsJournalSmAdv_getfactpoezdzakinfobyindex.sql

V2.55.0__2023-07-03_CZS2022-1244_update_stan_view_config_Дема.sql

R__wpsJournal_setReasonChangeLocomotive.sql R__wpsJournal_setReasonChangeLocomotiveBrig.sql V2.54.0__2023-07-03_CZS2022-1230_drop_SetReasonChangeLocomotive.sql

V2.53.0__2023-07-03_CZS2022-1225_add_columns.sql

R__wpsJournalSmAdv_putPoezdSheduleV2.sql

V2.52.0__2023-06-30_CZS2022-1219_new_index_mdl_tm_plan_op.sql

V2.51.0__2023-06-30_CZS2022-1206_mdl_tm_plan_loc_brig_op.sql

V2.50.0__2023-06-30_CZS2022-1226_update_nsi_cnsi_nsstanid.sql

V2.49.0__2023-06-29_CZS2022-1198_alter_pk.sql

R__gateCoreCar_setKategMestSost.sql

V2.48.0__2023-06-29_CZS2022-1198_alter_pk.sql

V2.47.0__2023-06-28_CZS2022-1063_mrm_task_updAt_null.sql V2.46.0__2023-06-27_CZS2022-1134_dictionary_coment.sql V2.45.0__2023-06-26_CZS2022-1063_mrm_task_2.sql V2.44.0__2023-06-26_CZS2022-1153_add_column_type_techPoly_KategMestSostRecIn.sql R__getCoreCar_setKategMestSost.sql R__techPoly_kategOp4100.sql R__techPoly_kategMestSost.sql

V2.43.0__2023-06-21_CZS2022-1063_mrm_task.sql

R__pnxArmTreeView_detBrigDepoObor.sql

V2.42.0__2023-06-21_CZS2022-1112_add_column_type_mdl_core2_tcontrolactioninfo.sql R__mdlCore2_getmodelcontrolactions.sql

V2.41.0__2023-06-21_CZS2022-1167_edit_table_nsi_movement_ban.sql

R__nsi_addNsiSortSysNapr.sql

V2.40.0__2023-06-16_CZS2022-1022_fix_side.sql

R__wZakrgraf_getRoleJson.sql

R__pnxLocoworkService_getParktotalsLok.sql

R__pnxArmTreeView_detBrigDepoObor.sql

R__pnxArmTreeView_detBrigDepoNar.sql

V2.39.0__2023-06-15_CZS2022-972_add_column_type_prost.sql R__prostCalcTdm_getTrbp.sql R__prostCalcTdm_getTr.sql R__prostCalcTdm_saveVagProst.sql

V2.38.0__2023-06-15_CZS2022-1106_add_nsi_attr.sql

R__pnxArmTreeView_DetOnWayLok.sql

R__mdlCore_doAfter.sql

V2.37.0__2023-06-14_CZS2022-1116_update_mng_sys__sys_options_catalog.sql

R__pnxArmTreeView_detBrigDepoNar.sql

V2.36.0__2023-06-14_CZS2022-1110_add_api_timer_setting.sql

R__pnxArmTreeView_detBrigDepoObor.sql

V2.35.0__2023-06-13_CZS2022-1096_add_column_mdl__ekasut_order_locomotive.sql

R__pnxArmTreeView_detBrigDepoNar.sql R__pnxArmTreeView_getNow.sql R__pnxArmTreeView_getLocInfo.sql R__pnxArmTreeView_getSostPark.sql R__pnxArmTreeView_getSostPark.sql R__pnxArmTreeView_getNpfList.sql

V2.34.0__2023-06-09_CZS2022-1028_depo_code_name.sql

V2.33.0__2023-06-06_CZS2022-1066_insert_pnx_permissions.sql

V2.32.0__2023-06-06_CZS2022-708_enable_tkp_cache.sql

V2.31.0__2023-06-05_CZS2022-1045_update_stan_view_config_Рузаевка.sql

V2.30.0__2023-05-31_CZS2022-1009_insert_new_rows_into_mng_sys_servers.sql

V2.29.0__2023-05-30_CZS2022-977_update_function_getprostdet_vag_tdm.sql

R__getCoreTrain_setTrainOperation.sql

V2.28.0__2023-05-12_CZS2022-976_update_stan_view_config_Входная.sql

V2.27.0__2023-05-12_CZS2022-946_add_column_tdm__prost_vag.sql

V2.26.0__2023-05-23_CZS2022-932_add_api_timer_setting.sql

V2.25.0__2023-05-22_CZS2022-913_change_comments.sql

V2.24.0__2023-05-22_CZS2022-932_add_column_tdm_park_occupation_calc.sql

V2.23.0__2023-05-22_CZS2022-898_insert_nsi_ns016.sql

V2.22.0__2023-05-19_CZS2022-948_update_api__systems_asust58_priority.sql

R__pnxArmTreeView_getNows.sql

V2.21.0__2023-05-18_CZS2022-924_drop_constrains_table_nsi__movement_ban.sql

V2.20.0__2023-05-18_CZS2022-849_alter_table_nsi__movement_ban.sql

V2.19.0__2023-05-17_CZS2022-905_add_api_timer_setting.sql

V2.18.0__2023-05-16_CZS2022-907_add_api_timer_setting.sql

V2.17.0__2023-05-16_CZS2022-676_create_table_cargo_transfer_tables.sql

V2.16.0__2023-05-16_CZS2022-904_add_api_timer_setting.sql

V2.15.0__2023-05-15_CZS2022-763_create_transfer_tables.sql

V2.14.0__2023-05-15_CZS2022-758_create_transfer_tables.sql

V2.13.0__2023-05-15_CZS2022-746_create_transfer_tables.sql

V2.12.0__2023-05-15_CZS2022-898_insert_nsi_ns016.sql

V2.11.0__2023-05-12_CZS2022-786_add_columns_emd_models__Xmd.sql

V2.10.0__2023-05-12_CZS2022-867_add_column_wps__wps_do24_norm_sort.sql

V2.9.0__2023-05-11_CZS2022-843_add_api_timer_setting.sql

V2.8.0__2023-05-11_CZS2022-849_create_table_nsi__movement_ban.sql

V2.7.0__2023-05-05_CZS2022-852_insert_nsi_ns016.sql

V2.6.0__2023-05-05_CZS2022-852_update_mng_sys__sys_options.sql

V2.5.0__2023-04-28_CZS2022-792_add_uniq_index_tdm_park_occupation_calc.sql

R__mdlCore2_getmodelcontrolactions.sql

V2.4.0__2023-04-29_CZS2022-827_alter_columns_nsi__rem_predp_lok.sql

V2.3.0__2023-04-29_CZS2022-699_create_table_nsi__nsi_transfer_plan_log.sql

V2.2.0__2023-04-28_CZS2022-792_create_table_tdm_park_occupation_calc.sql

V2.1.0__2023-04-28_CZS2022-699_create_nsi__stan_view_connected_park_transfer.sql

V1.512.0__2023-04-28_CZS2022-812_add_columns_nsi__rem_predp_lok.sql

V1.511.0__2023-04-28_CZS2022-805_insert_api__sys_options.sql

V1.510.0__2023-04-28_CZS2022-802_update_api__sys_options.sql

V1.509.0__2023-04-28_CZS2022-793_update_api__sys_options.sql

V1.508.0__2023-04-27_CZS2022-694_create_nsi_sort_sys_napr_transfer.sql

V1.507.0__2023-04-27_CZS2022-706_add_comment_mdl_ekasut_order.sql

V1.506.0__2023-04-26_CZS2022-795_update_api_timer_setting.sql

V1.505.0__2023-04-25_CZS2022-800_update_constraints.sql

V1.504.0__2023-04-24_CZS2022-671_create_table_asut_nsi_04_transfer.sql

V1.503.0__2023-04-24_CZS2022-668_create_table_asut_nsi_00_transfer.sql

V1.502.0__2023-04-24_CZS2022-759_add_systems.sql

V1.501.0__2023-04-24_CZS2022-791_add_systems.sql

V1.500.0__2023-04-21_CZS2022-666_create_table_nsi_st_obj_priznak_transfer.sql

V1.499.0__2023-04-21_CZS2022-664_create_table_ns_napr_podh_tbl_transfer.sql

V1.498.0__2023-04-20_CZS2022-745_wps_report_train_departure_alter_columns.sql

V1.497.0__2023-04-20_CZS2022-686_creation_table_nsi_station_mrm_integration_transfer.sql

V1.496.0__2023-04-20_CZS2022-730_insert_api_timer_setting.sql

V1.495.0__2023-04-19_CZS2022-756_update_stan_view_config_Боготол.sql

V1.494.0__2023-04-19_CZS2022-740_update_stan_view_config_СПб.Сорт.sql

V1.493.0__2023-04-18_CZS2022-747_mdl_ekasut_order_tps_type_id_drop_not_null.sql

V1.492.0__2023-04-18_CZS2022-727_refresh_tn_marsh_weights.sql V1.491.0__2023-04-18_CZS2022-727_refresh_tn_marsh_stik_sled.sql V1.490.0__2023-04-18_CZS2022-727_refresh_tn_marsh_rps.sql V1.489.0__2023-04-18_CZS2022-727_refresh_tn_marsh_plan.sql V1.488.0__2023-04-18_CZS2022-727_refresh_tn_marsh_owners.sql V1.487.0__2023-04-18_CZS2022-727_refresh_tn_marsh_otpr.sql V1.486.0__2023-04-18_CZS2022-727_refresh_tn_marsh_lengths.sql V1.485.0__2023-04-18_CZS2022-727_refresh_tn_marsh_break_st.sql

V1.484.0__2023-04-18_CZS2022-717_mng_sys_sys_options_insert_and_update.sql

V1.483.0__2023-04-17_CZS2022-737_add_uniq_index_to_shunting_job_engineman_station.sql

V1.482.0__2023-04-17_CZS2022-706_add_column_mdl_ekasut_order.sql

V1.481.0__2023-04-17_CZS2022-720_insert_nsi_ekasut_status.sql

V1.480.0__2023-04-14_CZS2022-719_tm_sek_alter_column.sql

V1.479.0__2023-04-14_CZS2022-703_add_api_timer_setting.sql

V1.478.0__2023-04-13_CZS2022-708_enable_tkp_cache.sql

V1.477.0__2023-04-12_CZS2022-649_add_api_timer_setting.sql

V1.476.0__2023-04-12_CZS2022-635_add_api_timer_setting.sql

V1.475.0__2023-04-12_CZS2022-701_delete_from_sys_options_and_timer_log.sql

V1.474.0__2023-04-12_CZS2022-687_tm_sek_op_alter_column.sql

V1.473.0__2023-04-11_CZS2022-636_add_comment_about_batch_size.sql

V1.472.0__2023-04-10_CZS2022-643_add_api_timer_setting.sql

V1.471.0__2023-04-06_CZS2022-636_add_column_batch_size_to_planner_task.sql

V1.470.0__2023-04-06_CZS2022-647_delete_rasp_tasks.sql

V1.469.0__2023-04-03_CZS2022-621_update_stan_view_config_Бабаево.sql

V1.468.0__2023-03-31_CZS2022-559_add_api_timer_setting.sql

V1.467.0__2023-03-30_CZS2022-615_corrections_tdm_prost_brig.sql

V1.466.0__2023-03-30_CZS2022-596_slov_obj_infr.sql

R__commonsPlsql_checkUserAndDb.sql

V1.465.0__2023-03-28_CZS2022-565_api_timer_setting.sql

V1.464.0__2023-03-28_CZS2022-558_create_table_prost_brig.sql

V1.463.0__2023-03-24_CZS2022-592_add_api_timer_setting.sql

V1.462.0__2023-03-23_CZS2022-567_add_api_timer_setting.sql

V1.461.0__2023-03-23_CZS2022-504_add_api_timer_setting.sql

V1.460.0__2023-03-22_CZS2022-584_update_stan_view_config_Бабаево.sql

V1.459.0__2023-03-22_CZS2022-457_create_analog_tables_from_emd_pp.sql

V1.458.0__2023-03-22_CZS2022-549_create_table_tdm_asov_data_calc.sql

R__pnxLocoworkService_getProstoyLokOnStan.sql R__pnxLocoworkService_getProstoyBrigOnStan.sql

V1.457.0__2023-03-17_CZS2022-532_api_timer_setting_updateCacheTkp.sql

V1.456.0__2023-03-17_CZS2022-494_create_table_prost_loc.sql

V1.455.0__2023-03-16_CZS2022-550_add_column_api.timer_setting.sql

V1.454.0__2023-03-16_CZS2022-509_update_stan_view_config_Бабаево.sql

R__pnxArmTreeView_getParkTotals.sql

V1.453.0__2023-03-01_CZS2022-501_update_wps_journal_sm_adv_factPoezdInfo.sql R__wpsJournalSmAdv_getFactPoezdInfo.sql R__wpsJournalSmAdv_getFactPoezdInfoByIndex.sql

R__gateAsutWej_processr306.sql

V1.452.0__2023-02-27_CZS2022-444_insert_api_sys_options.sql

R__mdlCore2_getmodelcontrolactions.sql

V1.451.0__2023-02-15_CZS2022-468_update_stan_view_config.sql

R__wPrn_htmlEscape.sql

V1.450.0__2023-02-10_CZS2022-449_change_planner_task_gruzp_scheduling.sql

V1.449.0__2023-02-10_CZS2022-372_change_planner_setting.sql

V1.448.0__2023-02-10_CZS2022-403_add_timer_task.sql

V1.447.0__2023-02-09_CZS2022-444_index_mrm_fn.sql

V1.446.0__2023-02-08_CZS2022-456_insert_operation_and_tpws_oper.sql

V1.445.0__2023-02-08_CZS2022-448_new_table_timetable_gruzp_res_rusch.sql

V1.444.0__2023-02-08_CZS2022-446_insert_nsi_ns015.sql

V1.443.0__2023-02-08_CZS2022-447_alter_columns_emd_models.sql

V1.442.0__2023-02-08_CZS2022-443_add_columns_to_emd_nsi_naim_depo.sql

V1.441.0__2023-02-07_CZS2022-425_new_table_ser_tps.sql

V1.440.0__2023-02-07_CZS2022-422_new_table_sost_lok.sql

V1.439.0__2023-02-07_CZS2022-414_new_table_order_kvd_routes.sql

V1.438.0__2023-02-07_CZS2022-412_insert_planner_settings.sql

V1.437.0__2023-02-07_CZS2022-445_api_timer_setting.sql

V1.436.0__2023-02-06_CZS2022-400_create_table_emd_park_put.sql

V1.435.0__2023-02-03_CZS2022-399_add_primary_key_and_index_asoup3_switch.sql

V1.434.0__2023-02-03_CZS2022-398_add_index_asoup3_dev_place.sql

V1.432.0__2023-02-03_CZS2022-396_add_primary_key_and_index_asoup3_railroad.sql

V1.431.0__2023-02-03_CZS2022-394_add_primary_key_and_index_asoup3_area.sql

V1.430.0__2023-02-03_CZS2022-390_add_primary_key_and_index_asoup3_classification.sql

V1.429.0__2023-02-03_CZS2022-390_add_primary_key_and_index_asoup3_organization_unit_new.sql

V1.428.0__2023-02-03_CZS2022-384_delete_sys_option.sql

V1.427.0__2023-02-02_CZS2022-393_add_rowid_asoup3.sql

V1.426.0__2023-02-02_CZS2022-423_add_api_timer_setting.sql

V1.425.0__2023-02-02_CZS2022-365_update_sys_options.sql

V1.424.0__2023-02-02_CZS2022-409_add_columns_to_tm_gir_mark.sql

V1.423.0__2023-02-02_CZS2022-365_add_api_timer_setting.sql

V1.422.0__2023-01-31_CZS2022-407_api_timer_setting.sql

V1.420.0__2023-01-24_CZS2022-373_new_schema_timetable.sql

V1.419.0__2023-01-24_CZS2022-382_update_nsstan_АЛТЫНКОЛЬ.sql

V1.418.0__2023-01-24_CZS2022-381_insert_nsi_dor.sql

V1.417.0__2023-01-24_CZS2022-389_update_stan_view_config.sql

V1.416.0__2023-01-24_CZS2022-384_add_columns.sql

V1.415.0__2023-01-18_CZS2022-368_add_row_id.sql

V1.414.0__2023-01-17_CZS2022-250_drop_old_getWaggonList.sql

V1.413.0__2023-01-16_CZS2022-358_add_row_ids.sql

V1.412.0__2023-01-16_CZS2022-359_correct_planner_settings.sql

V1.411.0__2023-01-13_CZS2022-364_update_stan_view_config.sql

R__pkgRepPlanFact_pipedPlanTrainsRow.sql

R__pnxArmTreeView_detBrigDepoObor.sql

V1.410.0__2023-01-11_CZS2022-356_update_stan_view_config.sql

V1.409.0__2023-01-11_CZS2022-352_deop_old_saveVVData.sql R__wpsJournalMd5_saveVVData.sql

V1.408.0__2023-01-10_CZS2022-333_alter_comments_kod_vp_to_mdl_train_schedule_tables.sql

V1.407.0__2023-01-10_CZS2022-327_drop_old_putPoezdShedule_v2.sql R__wpsJournalSmAdv_putPoezdSheduleV2.sql

V1.406.0__2023-01-10_CZS2022-333_add_kod_vp_to_mdl_train_schedule_tables.sql

R__pnxArmTreeViewPto_getOtzepCount.sql R__pnxArmTreeViewPto_getZoneInfo.sql

V1.405.0__2022-12-19_DRS2022-1862_alter_tm_norm_op12.sql

R__wpsSchemeProstPlan_getAllProstStan.sql

R__wpsJournalMd3_getDislocation.sql

R__wpsSchemeProstPlan_getAllProstStan.sql R__wpsSchemeProstPlan_getPeriodProstStanTdm.sql R__wpsSchemeProstPlan_getPlan.sql

V1.404.0__2022-12-12_CZS2022-252_create_table_tm_sek.sql

R__wpsSchemeProstPlan_getAllProstStan.sql

V1.403.0__2022-12-08_CZS2022-272_add_locomotive_operation.sql

V1.402.0__2022-12-06_EMDPP-370_emd_nsi.sql

del - V1.401.0__2022-12-06_CZS2022-233_add_m_view_skpp_report_plan_shipment.sql

del - V1.400.0__2022-12-06_CZS2022-222_add_m_view_tn_norm_pr_rem_lok.sql

del - V1.399.0__2022-12-06_CZS2022-225_add_m_view_tn_pf_opis_put_sled.sql

del - V1.398.0__2022-12-06_CZS2022-234_add_m_view_skpp_additional_services.sql

del - V1.397.0__2022-12-06_CZS2022-235_add_m_view_skpp_reason_failure.sql

del - V1.396.0__2022-12-05_CZS2022-224_add_m_view_tn_pf_put_sled_nazn.sql

del - V1.395.0__2022-12-05_CZS2022-235_add_m_view_b_sved_mm.sql

del - V1.394.0__2022-12-05_CZS2022-219_add_m_view_tn_norm_prob_lok.sql

V1.392.0__2022-12-02_EMDPP-362_replace_retention_policy.sql

V1.391.0__2022-12-02_resize_column_plan_form.sql

R__wpsJournal_getNameSer.sql

R__wpsJournal_getLocPodh.sql

V1.390.0__2022-12-01_PPLB-327_add_index_wej_asut_poezd.sql

R__pnxArmTreeView_detArrTrains.sql

R__wpsJournal_getNomLoc.sql R__wpsJournal_getNameSer.sql

V1.389.0__2022-11-25_EMDPP-362_emd_models_hypertable.sql

V1.388.0__2022-11-25_CZS2022-216_nsi_splmt.sql

R__mdlCore2_getModelTrains.sql

View R__dbq_VMessageResponse.sql

V1.387.0__2022-11-21_PPLB-298_nsrodvag_add_rod_vag_uch_asoup.sql

R__gateAsutWej_getAsutIdChain.sql R__gateAsutWej_getIdDoc303.sql R__gateAsutWej_getParam.sql R__gateAsutWej_getParamXml.sql R__gateAsutWej_Instr.sql R__gateAsutWej_processr303e.sql R__gateAsutWej_processr304.sql R__gateAsutWej_processr304e.sql R__gateAsutWej_processr305.sql R__gateAsutWej_sendAspplbZakaz.sql R__gateAsutWej_sendAsutCancelZakaz.sql R__gateAsutWej_version.sql

V1.386.0__2022-11-17_EMDPP-358_emd_worker_nsi.sql

V1.385.0__2022-11-17_DRS2022-1904_stan_view_config_Дема.sql

V1.384.0__2022-11-CZS2022-162_stan_view_config_Лиски.sql

V1.383.0__2022-11-15_DRS2022-1890_nsi_plan_form_add_column.sql

R__mdlCore2_getModelTrains.sql

R__techPoly_kategMestSost.sql

V1.382.0__2022-11-09_DRS2022-1885_stan_view_config.sql

R__pnxArmTreeView_getTrainCarsTotals.sql

V1.381.0__2022-11-08_EMDPP-346_change_pk_b_ser.sql

R__pnxArmTreeView_getTrainCarsTotals.sql

V1.380.0__2022-11-08_CZS2022-135_2_update_stan_view_config.sql

V1.379.0__2022-11-05_EMDPP-343_correct_table_emd_nsi_b_pasp.sql

V1.378.0__2022-11-05_CZS2022-123_update_api_settings.sql

V1.377.0__2022-11-04_CZS2022-114_add_dbq_28_dor_values.sql

V1.376.0__2022-11-04_CZS2022-111_add_station_mrm_integration.sql

V1.375.0__2022-11-03_CZS2022-124_correct_table.sql

V1.374.0__2022-11-03_CZS2022-119_update_nsi_splmt.sql

V1.373.0__2022-11-03_CZS2022-109_add_timer_setings_for_clearReportTrainDepartureData.sql

V1.372.0__2022-11-02_CZS2022-94_add_timer_setting.sql

V1.371.0__2022-11-01_CZS2022-93_add_column.sql

V1.370.0__2022-11-01_CZS2022-102_add_nsi_splmt.sql

V1.369.0__2022-11-01_CZS2022-100_add_pnx_permissions.sql

V1.368.0__2022-01-01_CZS2022-93_size_column.sql

V1.367.0__2022-01-01_CZS2022-93_create_new_table.sql

V1.366.0__2022-10-31_CZS2022-79_drop_old_version_function.sql R__wpsAnalysisDeparturet2_cancelZakazAsut.sql

V1.365.0__2022-10-31_EMDPP-338_update_tables_emd_nsi.sql

R__gateAsutWej_processr306.sql

V1.364.0__2022-10-31_CZS2022-93_correct_report_detail_vid.sql

V1.363.0__2022-10-31_CZS2022-78_drop_old_version_function.sql R__gateAsutWej_sendAsutZakaz.sql R__wpsJournal_sendZakazAsut.sql

V1.362.0__2022-10-31_CZS2022-77_wej_asut_lok_brig_history.sql

V1.361.0__2022-10-30_CZS2022-93_create_new_table.sql

V1.360.0__2022-10-28_CZS2022-78_drop_old_version_function.sql R__wpsJournal_sendZakazAsut.sql R__gateAsutWej_sendAsutZakaz.sql

V1.359.0__2022-10-28_CZS2022-77_add_table_wej_asut_lok_brig_history.sql

V1.358.0__2022-10-28_CZS2022-90_add_timer_setings_for_clearAsutOrderData.sql

V1.357.0__2022-10-27_EMDPP-333_update_planner_settings.sql

V1.356.0__2022-10-27_EMDPP-334_update_tables_emd_nsi.sql

R__prostCalcTdm_getProstOtpr.sql

V1.355.0__2022-10-25_CZS2022-70_drop_column_tm_norm_op_gg.sql

V1.354.0__2022-10-07_DRS2022-1838_update_stan_view_config_Металлургическая.sql

V1.353.0__2022-10-19_CZS2022-55_create_table_priority_park.sql

V1.352.0__2022-10-18_DRS2022-1836_delete_row_nsi_tmo.sql

V1.351.0__2022-10-18_CZS2022-45_add_columns_nsi_park_and_put.sql

V1.350.0__2022-10-CZS2022-42_add_pnx_permissions.sql

V1.349.0__2022-10-18_DRS2022-1756_upload_table_sync_table.sql

V1.348.0__2022-10-17_DRS2022-1833_create_table_sync_table.sql

V1.347.0__2022-10-07_DRS2022-1798_update_nsi_tpws_protz_fn.sql

V1.346.0__2022-10-07_DRS2022-1807_update_stan_view_config_Карталы_1.sql

V1.345.0__2022-10-06_CZS2022-33_add_timer_setings_for_clearEkasutOrderData.sql

V1.344.0__2022-10-05_DRS2022-1801_asoup-broker_SPN4699_TIMEZ.sql

V1.343.0__2022-10-05_CZS2022-31_add_timer_setings_for_clearReportStandardDeviation.sql

V1.342.0__2022-10-05_CZS2022-24_add_table_wps_report_standard_deviation.sql

V1.341.0__2022-10-05_PPLB-1_add_pnx_permissions.sql

R__pnxSidebarAsuds_getWaggonList.sql

V1.340.0__2022-10-03_DRS2022-1791_update_stan_view_config_Карталы1_Металлургическая.sql

V1.339.0__2022-09-30_DRS2022-1787_add_table_nsi_tmo_del.sql

R__gateAsutWej_getResponce.sql

V1.338.0__2022-09-29_EMDPP-295_new_schema_emd_nsi.sql

V1.337.0__2022-09-21_CZS2022-18_add_columns_ekasut_order_locomotive.sql

V1.336.0__2022-09-21_CZS2022-11_add_ekasut_order_normative.sql

DRS2022-1767 R__clearDbPoly_closeOldTrain.sql

V1.335.0__2022-09-21_DRS2022-1766_add_index.sql

V1.334.0__2022-09-19_add_mdl_props_saveSizeBatch.sql

V1.333.0__2022-09-01_create_index_mrm_fn_task.sql

R__gateCoreCar_processVagOperation.sql

R__pnxLocoworkService_getParktotalsLok.sql R__pnxArmTreeView_DetOnWayLokLongStand.sql R__pnxArmTreeView_DetOnWayLok.sql R__pnxArmTreeView_DetOnWayBrig.sql R__pnxLocoWorkService_getLongStandInDepo.sql

R__gateAsutWej_sendAsutZakaz.sql

R__mdlCore2_getModelCars.sql R__mdlCore2_getModelCarsTrain_connection.sql R__mdlCore2_getModelCarsTrain_new.sql

V1.332.0__2022-09-01_update_stan_view_config_Октябрьск.sql

R__mrmZakr_poezdNorm.sql R__mrmZakr_zakrSave.sql R__wZakrgraf_getIdTraZakPutV3.sql R__wZakrgraf_getPoezdSost.sql

R__nsi_technologicalProcessEditorProcessView.sql

R__prostCalcTdm_getVagAtrProstCalc.sql

V1.332.0__2022-08-27_tm_vag_op_update_dis_esr.sql - удален

R__prostCalcTdm_getProstOtpr.sql

V1.331.0__2022-08-25_update_stan_view_config_Рузаевка.sql

R__gateAsutWej_sendAsutZakaz.sql

R__pnxLocoworkService_getParktotalsLok.sql

V1.330.0__2022-08-25_update_stan_view_config_СанктПетербСорт.sql V1.329.0__2022-08-25_update_stan_view_config_Рузаевка.sql

R__pnxArmTreeView_getParkTotals.sql

V1.328.0__2022-08-24_correct_penza_config.sql

V1.327.0__2022-08-24_add_operation_ns015.sql

R__wZakrgraf_doLogonJson.sql

R__wZakrgraf_getZakrInfoJson R__wZakrgraf_getZakrInfoJson.sql

R__wZakrgraf_getMestHrJson.sql R__wZakrgraf_getPutPoezdChetBashJson.sql R__wZakrgraf_getPutPoezdVagBashJson.sql R__wZakrgraf_getZakJur.sql R__wZakrgraf_prnPutBash.sql R__wZakrgraf_prnPutBash3.sql R__wZakrgraf_prnPutBashLok3.sql R__wZakrgraf_prnPutBashLokV2.sql

R__wZakrgraf_getPoezdSost.sql R__wZakrgraf_getZakrInfoJson.sql

V1.326.0__2022-08-08_EMDPP-151_add_emd_models.sql

V1.325.0__2022-05-25_drop_not_null_on_request_log_id_in_process_log.sql

R__clearDbPoly_closeOldTrain.sql

R__mdlCore2_getmodelcontrolactions.sql

R__wpsFormPkg_getRoleOpop.sql R__wpsFormPkg_getRolePikop.sql

V1.324.0__2022-08-05_DRS2022-1517_tn_pto_vag.sql

V1.323.0__2022-08-02_EMDPP-131_correct_planner_task.sql

R__clearDbPoly_closeOldTrain.sql

V1.322.0__2022-08-02_EMDPP-131_add_planner_task.sql

V1.321.0__2022-08-01_DRS2022-1557_stan_view_config.sql

V1.320.0__2022-07-30_EMDPP-130_insert_timer_setting_emdPlaner.sql

V1.319.0__2022-07-30_EMDPP-129_new_schema_planner.sql

R__wpsPkgThrdFilterCheck_getNsiThrdFilter.sql V1.318.0__2022-07-27_DRS2022-1536_reload_filter_thread.sql

V1.317.0__2022-07-25_DRS2022-1523_stan_view_config_to_jsonb.sql

V1.316.0__2022-07-25_DRSI21-266_graf_peregon.sql

R__wpsJournal_notKodopLok.sql

R__mdlcore2_getmodelplantrains.sql V1.315.0__2022-07-22_DRS2022-1495_add_attribute_type.sql R__mdlCore2_getModelTrains.sql R__mdlCore2_getModelShedule.sql

R__gateCore_getCloseTrainRecState.sql R__gateCore_checkCloseTrain.sql R__gateCore_tryGetTrain.sql

V1.314.0__2022-07-19_DRS2022-1401_insert_data_timer_setting.sql

R__pnxSidebarAsuds_getTrainVags.sql R__pnxSidebarAsuds_getWaggonList.sql

R__wpsStationView_getStationTree.sql

V1.313.0__2022-07-14_correct_legend_tkp.sql

R__pnxArmTreeView_getLocInfo.sql

R__armTreeGrafika_getSummTrainVags.sql R__armTreeGrafika_prd.sql R__armTreeGrafika_isd.sql

V1.312.0__2022-07-12_DRS2022-1378_add_rmrm_integration.sql

R__techPoly_kategKach.sql

R__techPoly_kategMestSost.sql

V1.311.0__2022-07-11_DRS2022-1406_add_road.sql

R__tdm_calcLokChange.sql

V1.310.0__2022-07-08_DRS2022-1295_add_stan_pr_lok.sql

R__getCoreCar_processVagOperation.sql

V1.309.0__2022-07-07_DRS2022-1338_nsvag_nstiprod.sql

V1.308.0__2022-07-06_DRS2022-1295_add_stan_pr_lok.sql

V1.307.0__2022-07-04_DRS2022-1364_drop_index_xak1report_plan_fact_mailing.sql

R__pnxLocoworkService_getProstoyLok.sql

R__clearDbPoly_clearHardTdmTabs.sql

R__gateCoreCar_setKategMestSost.sql

R__mdlCore2_getModelLocomotiveCrew.sql

R__getCoreTrain_insertUpdateTmObjects.sql

R__techPoly_prMestOp30t08091541t0102.sql

V1.305.0__2022-06-24_DRS2022-1237_add_dbq_24_dor.sql

V1.304.0__2022-06-23_DRS2022-1280_add_index_mrm_fn_task.sql

R__pnxArmTreeViewPto_getCrewPos.sql

V1.303.0__2022-06-16_DRS2022-846_set_uniq_field.sql

V1.302.0__2022-06-15_DRS2022-1201_nsi_nstiprod.sql

V1.301.0__2022-06-14_DRS2022-846_modify_tables.sql

V1.300.0__2022-06-14_DRS2022-1040_insert_status_job.sql

V1.299.0__2022-06-14_DRS2022-847_insert_new_system.sql

V1.298.0__2022-06-14_DRS2022-846_create_new_tables.sql

V1.297.0__2022-06-10_DRS2022-1191_update_nsstan_810141.sql

R__pnxArmTreeView_getPeregonEsr.sql

V1.296.0__2022-06-10_DRS2022_1184_drop_columns_nsi_park.sql

V1.295.0__2022-06-10_create_table_nsi_nsvag.sql

V1.294.0__2022-06-09_DRS2022-971_create_table_report_plan_fact_counted.sql

V1.293.0__2022-06-07_DRS2022-1069_insert_data_timer_setting.sql

V1.292.0__2022-06-07_DRS2022-1080_sys_options_catalog.sql

V1.291.0__2022-06-06_DRS2022-1157_update_nsstan_810141.sql

V1.290.0__2022-06-06_DRS2022-1138_insert_nsgruz_group.sql

V1.289.0__2022-06-06_DRS2022-1093_add_operations.sql

V1.288.0__2022-06-01_DRS2022-1110_alter_report_plan_fact.sql

V1.287.0__2022-06-01_DRS2022-1125_sys_options_catalog.sql

V1.286.0__2022-05-31_DRS2022-693_alter_table_add_columns_nsi.sql

V1.285.0__2022-05-30_DRS2022-697_create_table_api_integration_emd_log.sql

V1.284.0__2022-05-30_DRS2022-691_nsi.technological_process_editor_process_view-create.sql

V1.283.0__2022-05-27_DRS2022-689_alter_table_add_columns_nsi.sql

V1.282.0__2022-05-27_DRS2022-678_insert_api_systems.sql

V1.281.0__2022-05-25_DRS2022-1030_update_timer_setting.sql

V1.280.0__2022-05-25_DRS2022-1030_insert_sys_option.sql

R__mdl_stanParkPrioritet.sql R__mdl_roadParkPrioritet.sql R__mdl_parkPrioritet.sql

V1.279.0__2022-05-24_DRS2022-1058_insert_sys_option.sql

V1.278.0__2022-05-23_DRS2022-1038_insert_sys_options.sql

V1.277.0__2022-05-20_update_mdl_props.sql

V1.195.1__2022-05-18_DRS2022-989_fix_ns_kateg_poezd_and_ls004.sql Исправление для наката на чистую базу

V1.276.0__2022-05-20_DRS2022-1031_alter_type_report_plan_fact_mailing_schedule_log.sql

V1.275.0__2022-05-16_DRS2022-980_alter_report_type.sql

V1.274.0__2022-05-14_create_index_mrm_fn_task.sql R__wpsSchemeMrm_getIdleTask.sql

R__mrmZakr_calcNorm.sql R__mrmZakr_calcNormInt.sql R__mrmZakr_getIdTra.sql R__mrmZakr_getPrchetZakr.sql R__mrmZakr_getVagBashItem.sql

V1.273.0__2022-05-12_DRS2022-956_trim_crlf_name_ob.sql

V1.272.0__2022-05-12_DRS2022-811_update_report_type.sql

V1.271.0__2022-05-12_DRS2022-941_insert_priznak_0410_EXECUTION_CONTROL_MRM.sql

V1.270.0__2022-05-12_DRS2022-943_alter_report_plan_fact.sql

R__gateAsutWej_processr306.sql

R__wpsJournal_sendZakazAsut.sql V1.269.0__2022-05-11_DRS2022-915_drop_sendzakazasut.sql

V1.268.0__2022-05-11_DRS2022-869_insert_mrm_sch_and_update_put.sql

V1.267.0__2022-05-10_DRS2022-899_update_comment_wps_schedule_log_table.sql

V1.266.0__2022-05-10_DRS2022-898_insert_station_mrm_integration.sql

V1.265.0__2022-05-09_DRS2022-787_enable_new_theme.sql

V1.264.0__2022-05-08_DRS2022-522_api_sys_options_fix_cargo_option_names.sql
Названия опций грузовой работы в UPPER CASE.

V1.263.0__2022-05-06_DRS2022-873_cargo_client_carriage_operation_archive_drop_foreign_key.sql
Небольшая корректировка в схеме cargo.

V1.262.0__2022-05-05_add_column_mdl_vagon_group_id.sql

V1.261.0__2022-05-05_DRS2022-842_update_nsi_splmt_20207.sql

V1.260.0__2022-05-04_DRS2022-436_insert_data_timer_setting.sql

V1.259.0__2022-05-04_DRS2022-843_insert_data_timer_setting_and_alter_columns.sql

V1.258.0__2022-05-04_DRS2022-811_alter_report_category.sql

V1.257.0__2022-05-03_DRS2022-814_alter_report_plan_fact_mailing.sql

V1.256.0__2022-05-03_create_table_mailing_schedule_log.sql

V1.255.0__2022-05-03_DRS2022-794_add_splmt.sql

V1.254.0__2022-05-03_DRS2022-814_alter_report_plan_fact_mailing.sql

V1.253.0__2022-05-04_DRS2022-811_new_report_category_and_type.sql

V1.252.0__2022-05-03_DRS2022-815_trim_nameob.sql

V1.251.0__2022-05-02_view_nsi_kateg.sql

R__mrmZakr_calcNormInt.sql

V1.250.0__2022-04-29_create_table_log_mailing_sender.sql

V1.249.0__2022-04-28_add_dbq_01_58_63_80_add_dbq_extcall_83_88_92_94_96.sql

V1.248.0__2022-04-28_fixbug_create_table_vagon_group.sql

V1.247.0__2022-04-28_DRS2022-788_podhod_mdl.sql

V1.246.0__2022-04-27_DRS2022-705_add_column.sql

V1.245.0__2022-04-26_DRS2022-436_add_index_tm_lok_brig_op.sql

V1.244.0__2022-04-27_DRS2022-781_enable_new_theme.sql
Новая цветовая схема.

V1.243.0__2022-04-25_DRS2022-716_correct_table_report_plan_fact.sql

V1.242.0__2022-04-22_update_ns_kateg_poezd_mdl.sql

V1.241.0__2022-04-20_correct_table_report_plan_fact.sql

V1.240.0__2022-04-19_DRS2022-48_new_table_nsi_vid_rab_kateg.sql

V1.239.0__2022-04-18_DRS2022-666_insert_station_mrm_integration.sql

V1.238.0__2022-04-18_DRS2022-622_add_comment.sql

V1.237.0__2022-04-15_DRS2022-612_add_timer_setings.sql

V1.236.0__2022-04-15_DRS2022-642_add_splmt.sql

V1.235.0__2022-04-11_DRS2022-25_new_table_log_control_execution.sql

V1.234.0__2022-04-11_DRS2022-27_add_columns.sql

V1.233.0__2022-04-11_DRS2022-26_insert_pnx_permissions.sql

V1.232.0__2022-04-11_DRS2022-20_insert_nsi_attribute.sql

V1.231.0__2022-04-07_DRS2022-578_add_column_mdl_ekasut_order.sql

V1.230.0__2022-04-07_DRS2022-575_add_api_systems.sql

V1.229.0__2022-04-07_DRS2022-271_alter_column_type.sql

R__pnxArmTreeView_detVsCars.sql

V1.228.0__2022-04-06_DRS2022-574_add_api_systems.sql

V1.227.0__2022-04-06_DRS2022-516_delete_20090_from_nsi_splmt.sql

V1.226.0__2022-04-05_DRS2022-252_create_report_mailing.sql Добавлены таблицы для хранения настроек рассылки

R__clearDbPoly_clearMdlTabs.sql

V1.225.0__2022-04-04_DRS2022-360_cargo_limit_correction_add_foreign_key.sql Грузовая работа. Добавлен внешний ключ в cargo.limit_correction.

R__gateCoreCar_processVagOperationJson.sql

V1.224.0__2022-04-01_DRS2022-360_cargo_carriage_group_correction_fix_comment.sql Грузовая работа. Исправлен комментарий.

V1.223.0__2022-04-01_DRS2022-371_cargo_dictionaries_update.sql

V1.222.0__2022-03-31_DRS2022-520_cargo_cleanup.sql Грузовая работа. Параметры чистки таблиц по грузовой работе.

V1.221.0__2022-03-30_DRS2022-211_add_pnx_permissions.sql

V1.220.0__2022-03-30_DRS2022-413_add_table_report_plan_fact.sql

V1.219.0__2022-03-29_DRS2022-514_alter_column_PPR_PTO_BRIG.sql Изменен тип с int8 на numeric поля id_obj таблицы nsi.PPR_PTO_BRIG

R__pnxArmTreeView_getParkTotals.sql R__pnxArmTreeView_getHumpWork.sql R__pnxArmTreeView_detVsTrains.sql R__pnxArmTreeView_detVsCars.sql R__pnxArmTreeView_detRasfTrains.sql R__pnxArmTreeView_detRasfCars.sql

V1.218.0__2022-03-28_DRS2022-514_correct_nsi_tpws_oper_protz.sql

V1.217.0__2022-03-26_DRS2022-438_insert_data_ns015.sql Добавлены данные в таблицу nsi.ns015

R__clearDbPoly_clearHardTdmTabs.sql

R__mdlCore2_getModelShedule.sql R__mdlCore2_getModelCarsTrain_new.sql R__mdlCore2_getModelCars.sql

V1.216.0__2022-03-24_DRS2022-295_alter_pk.sql Могут поступать несколько записей по одной бригаде. Исправлен составной ключ

V1.215.0__2022-03-23_DRS2022-450_cargo_way_storage_table.sql
Грузовая работа. Таблица для хранения информации о путях накопления.

V1.214.0__2022-03-23_DRS2022-401_cargo_permissions.sql
Грузовая работа. Права доступа пользователя к осуществлению работы с грузовой работой.

V1.213.0__2022-03-23_DRS2022-398_cargo_limit_tables.sql
Грузовая работа. Таблицы по вводу лимита корректировок для диспетчера и клиента.

V1.212.0__2022-03-23_fix_gg_time_index.sql

R__armTreeGrafika_dko.sql Добавлена проверка на 30 дней

V1.211.0__2022-03-23_DRS2022-334_cargo_carriage_tables.sql Грузовая работа. Таблицы по вагонам для введения вагонной модели у клиента.

V1.210.0__2022-03-23_DRS2022-371_cargo_dictionaries.sql Грузовая работа. Заполнение информации в справочные таблицы.

V1.209.0__2022-03-23_DRS2022-360_cargo_carriage_group_tables.sql
Грузовая работа. Таблицы по группам вагонов, участвующих в грузовой работе.

V1.208.0__2022-03-21_DRS2022-4_new_pnx_permissions.sql

V1.207.0__2022-03-18_DRS2022-284_cargo_client_information_tables.sql
Грузовая работа. Таблицы для информации по клиентам.

R__mdlCore2_getModelLocomotiveCrew.sql

V1.206.0__2022-03-17_DRS2022-453_dop_tn_marsh_tables.sql

V1.205.0__2022-03-17_DRS2022-359_delete_old_cargo_work_tables.sql
Старые таблицы грузовой работы. Будут заменены схемой cargo.

V1.204.0__2022-03-17_DRS2022-426_correct_nsi_tpws_oper_protz.sql

V1.203.0__2022-03-16_DRS2022-122_cargo_work_missing_user_agreements_tables_comments.sql

V1.202.0__2022-03-16_DRS2022-122_cargo_work_missing_user_agreements_tables.sql

R__mdlCore2_getModelLocomotiveCrew.sql

V1.201.0__2022-03-14_DRS2022-8_update_pnx_permissions.sql

V1.200.0__2022-03-09_DRS2022-8_insert_delete_pnx_permissions.sql

V1.199.0__2022-03-09_DRS20211-1744_delete_constraint.sql

V1.198.0__2022-03-09_DRS2022-378_update_code_operation.sql

V1.197.0__2022-03-05_DRS2022-273_insert_data_timer_setting.sql

V1.196.0__2022-03-04_DRS2022-259_kateg_2021.sql

V1.195.0__2022-03-04_DRS2022-374_wps_template_fix_not_null.sql

V1.194.0__2022-03-04_DRS2022-244_remove_eco_tomsk_cleanup_options.sql

V1.193.0__2022-03-03_DRS2022-83_skpp_reason_comment.sql

V1.192.0__2021-03-03_DRS20211-1742_commercial_malfunctions_comment.sql

R__wZakrgraf_getParkTime.sql R__wZakrgraf_getSmenaJson.sql R__wZakrgraf_getZakPerech.sql R__wZakrgraf_getZakrInfoJson.sql R__wZakrgraf_stInfoJson.sql

V1.191.0__2022-03-01_DRS2022-244_add_eco_tomsk_cleanup_options.sql

V1.190.0__2022-03-01_DRS2022-356_correction_ns_kateg_poezd_mdl.sql

V1.189.0__2022-03-01_DRS2022-254_wps_message_add_column.sql

V1.188.0__2022-03-01_DRS2022-348_change_fkey_nsi_task.sql

V1.187.0__2022-02-25_DRS2022-83_table_nsi_skpp_fix.sql

V1.186.0__2022-02-25_DRS2022-83_table_nsi_skpp_add_index.sql

V1.185.0__2022-02-28_DRS2022-254_template_object_types.sql

R__mdlCore2_getModelLocomotiveCrew.sql V1.184.0__2022-02-25_DRS2022-312_create_type.sql

V1.183.0__2022-02-25_DRS2022-310_drop_table_brig_recreation.sql

V1.182.1__2022-02-25_DRS2022-35_fix_tpws_kodop_move_sn.sql V1.182.0__2022-02-25_DRS2022-35_fn_update.sql

R__wZakrgraf_checkVsvopSave.sql R__wZakrgraf_clearZakrJson.sql R__wZakrgraf_saveCheck.sql R__wZakrgraf_saveZakrJson.sql

V1.181.0__2022-02-25_DRS2022-83_create_table_nsi_skpp.sql

V1.180.0__2022-02-24_DRS2022-310_create_table_brig_recreation.sql

V1.179.0__2022-02-24_mdl_opcode_matching.sql

V1.178.0__2022-02-23_DRS2022-254_wps_message.sql

V1.177.0__2022-02-22_DRS2022-326_rename_table_nsi_attribute_to_nsi_ppss_attribute.sql

V1.176.0__2022-02-22_DRS2022-259_new_kateg_mdl.sql

V1.175.0__2022-02-21_DRS20211-1344_alter_table.sql

R__getCoreCar_setKategMestSost.sql

R__clearDbPoly_runClear.sql R__clearDbPoly_clearPpss.sql V1.174.0__2022-02-17_create_index_table_ppss.sql

V1.173.0__2022-02-17_DRS2022-163_rename_table_nsi_ppss_attribute_to_nsi_attribute.sql V1.172.0__2022-02-17_DRS2022-141_insert_mng_sys_pnx_permissions.sql V1.171.0__2022-02-17_DRS2022-140_insert_mng_sys_pnx_permissions.sql V1.170.0__2022-02-17_DRS2022-39_add_columns_mgs_sys_role.sql V1.169.0__2022-02-17_DRS2022-38_add_column_asuds_mrm_fn_ispol.sql

V1.168.0__2022-02-15_DRS2022-266_zakr_spis.sql

R__wpsJournalSmAdv_getfactpoezdzakinfobyindex.sql

R__tdm_triggerTmVagDeleteTrg.sql R__nsi_nsiUpdateRecData.sql

R__gateFix_syncTmObjectTables.sql

V1.167.0__2022-02-08_DRS20211-1344_create_tm_plan_loc_brig_op.sql

R__clearDbPoly_clearHistoryLog.sql R__tdm_triggerTmVagDeleteTrg.sql R__nsi_nsiUpdateRecData.sql

V1.166.0__2022-02-09_DRS20211-1607_tmp_commercial_malfunctions_add_comment.sql

R__armTreeView_printpodhodcontent.sql

V1.165.0__2022-02-08_DRS20211-1344_create_tm_plan_loc_brig_op.sql

R__armTreeView_printpodhodgrafcontent.sql

R__wpsJournal_getLocStan.sql R__wpsJournal_getLocPodh.sql R__wpsJournal_getLocStanHand.sql

R__pnxArmTreeView_getHumpWork.sql

V1.164.0__2022-02-01_DRS2022-107_create_process_log.sql

V1.163.0__2022-01-28_DRS2022-136_create_tables_mng_sys_servers.sql

V1.162.0__2022-01-27_DRS2022-118_order_weight_length_not_null.sql V1.161.0__2022-01-27_DRS2022-120_order_kvd_routes_not_null.sql V1.160.0__2022-01-27_DRS2022-118_order_weight_length_not_null.sql

R__wpsJournal_getLocStan.sql

R__mrmZakr_calcNorm.sql

V1.159.0__2022-01-20_DRS20211-1680_tm_lok_brig_weekends_comments.sql

R__tdm_createViewOrderKvdRoutes1.sql R__tdm_createViewOrderKvdRoutes2.sql R__tdm_resetSeqOrderKvdRoutes1.sql R__tdm_resetSeqOrderKvdRoutes2.sql V1.158.0__2022-01-20_DRS2022-79_create_order_kvd_routes_tables.sql

V1.157.0__2022-01-19_DRS20211-1680_create_tm_lok_brig_weekends_table.sql

R__tdm_resetSeqOrderWeightLength1.sql R__tdm_resetSeqOrderWeightLength2.sql R__tdm_createViewOrderWeightLength1.sql R__tdm_createViewOrderWeightLength2.sql V1.156.0__2022-01-19_DRS2022-42_create_order_weight_length_tables.sql

V1.155.0__2022-01-18_DRS20211-1677_insert_data_timer_setting.sql

V1.154.0__2022-01-18_DRS20211-1677_create_timer_table.sql

V1.153.0__2022-01-17_DRS20211-1674_add_changeChoise_to_service_endpoint.sql

V1.152.0__2022-01-14_DRS2022-30_add_loc_116.sql

R__pnxArmTreeView_detArrTrains.sql R__pnxArmTreeView_detDepTrains.sql

V1.151.0__2021-12-27_DRS20211-1642_tm_object_op_add_index.sql

V1.150.0__2022-01-13_DRS2022-30_update_nsi_cnsi_lok_addition_ser.sql

V1.149.0__2022-01-12_DRS20211-1502_update_output_norm_depo.sql

V1.148.0__2022-01-12_DRS20211-1665_fix_comments.sql

R__pnxArmTreeView_getParkTotals.sql

V1.147.0__2022-01-11_DRS20211-1502_remove_output_norm_home_depot_station.sql

V1.146.0__2022-01-11_DRS20211-1502_alter_output_norm_group.sql

V1.145.0__2022-01-11_DRS20211-1518_add_comments.sql

V1.144.0__2022-01-11_DRS20211-647_update_table.sql

R__mdlCore2_getmodelcontrolactions.sql

V1.143.0__2021-12-27_DRS20211-1345_alter_column.sql

R__pnxArmTreeView_getSostPark.sql

V1.142.0__2021-12-24_DRS20211-1638_add_error_column.sql

R__clearDbPoly_clearMappingGateTdm.sql

V1.140.0__2021-12-21_DRS20211-973_fix_text_name.sql

R__gateCore_tryGetTrainOldNew.sql

V1.139.0__2021-12-21_DRS20211-1339_change_type.sql

V1.138.0__2021-12-20_DRS20211-1585_update_pk_delete_duplicate.sql

R__armTreeGrafika_nrp.sql

V1.137.0__2021-12-20_DRS20211-1557_order_status_constraint.sql

V1.136.0__2021-12-17_DRS20211-1557_add_new_option_value.sql

R__pnxArmTreeView_getSostPark.sql

R__clearDbPoly_clearMappingGateTdm.sql

R__wpsJournalSmAdv_getNaznInNormV5.sql

V1.135.0__2021-12-16_update_or_insert_table_nsi_gir-nsi-02.sql

V1.134.0__2021-12-15-DRS20211-1224_fix_duble.sql

V1.133.0__2021-12-14-DRS20211-1545_correct_role.sql

R__pnxArmTreeView_getParkOccupation.sql R__pnxArmTreeView_getParkTotals.sql R__pnxArmTreeViewPto_getCrewBusy.sql R__pnxArmTreeViewPto_getCrewPos.sql R__pnxArmTreeViewPto_getparkOccupationPto.sql R__pnxArmTreeViewPto_getParkTotalsPto.sql R__pnxLocoWorkService_getLongStandInDepo.sql R__pnxLocoWorkService_getParkOccupationLok.sql R__pnxLocoWorkService_getParkTotalsLok.sql R__pnxLocoWorkService_getProstoyLok.sql R__wpsShemeProst_getProstSysFaktTdm.sql R__wpsShemeProst_getStationTabloCompareTr.sql R__wpsShemeProst_getStationTabloCompareTrDo24.sql R__wpsStationView_getIdPark.sql R__wpsStationView_getStationTree.sql

V1.132.0__2021-12-13_add_index_tm_lok_brig_op.sql

R__pnxArmTreeView_getPodhodList.sql

V1.131.0__2021-12-10-DRS20211-1495_mrm_fn_wagons_vu23.sql

V1.130.0__2021-12-09-DRS20211-1481_new_roles.sql

R__pnxArmTreeView_getParkTotals.sql

V1.129.0__2021-12-08-DRS20211-1449_update_comment.sql

V1.128.0__2021-12-07-DRS20211-1374_cnsi_loc_depo_org_size.sql

V1.127.0__2021-12-06-DRS20211-1407_type_n_func.sql R__mdlcore2_getfacttrainbyid.sql R__mdlcore2_getmodelplantrains.sql R__mdlcore2_getmodeltrainsoed.sql R__mdlcore2_getmodeltrains.sql

R__tdm_calcWeekNarabotki.sql V1.126.0__2021-11-30_DRS20211-1322_correct_timer_task.sql R__mdlCore2_getModelShedule.sql

R__wZakrgraf_getRoleJson.sql R__wZakrgraf_prnRight.sql R__wZakrgraf_prnRoleJson.sql

R__wZakrgraf_getRoleJson.sql R__wZakrgraf_getZakPerech.sql

R__clearDbPoly_clearMappingGateTdm.sql R__clearDbPoly_clearHardTdmTabs.sql R__clearDbPoly_runClear.sql

V1.125.0__2021-12-04_DRS20211-1359_edit_type_column_marks.sql

R__wZakrgraf_getMestHrJson.sql R__wPrn_prnElapsed.sql R__wZakrgraf_prnMestHrPark.sql

V1.124.0__2021-12-02_DRS20211-1345_create_table.sql

R__wpsJournal_sendZakazAsut.sql

V1.123.0__2021-12-02_DRS20211-1349_WEJ_ASUT_POEZD_add_column_idthread.sql

V1.122.0__2021-12-02_DRS20211-1363_thread_id_for_ekasut_order.sql

V1.121.0__2021-11-30_DRS20211-1322_correct_timer_task.sql

V1.120.0__2021-11-30_DRS20211-829_ekasut_order_status_alter_constraint.sql

V1.119.0__2021-11-30_DRS20211-1322_create_timer_task.sql

V1.118.0__2021-11-30_DRS20211-1321_create_table_tm_lok_brig_weeknarabotki.sql

V1.117.0__2021-11-30_DRS20211-1296_update_option.sql

V1.116.0__2021-11-30_DRS20211-1335_alter_columns.sql

V1.115.0__2021-11-30_DRS20211-1296_add_new_option_values.sql

V1.114.0__2021-11-30_DRS20211-1197_add_new_permissions.sql R__mdlCore2_getModelCars.sql R__mdlCore2_getModelShedule.sql R__wpsFormPkg_getRoleOpop.sql R__wpsFormPkg_getRolePikop.sql

V1.113.0__2021-11-24_DRS20211-1336_alter_column.sql

V1.112.0__2021-11-24_DRS20211-1326_alter_columns.sql

V1.111.0__2021-11-29_DRS20211-1287_MDL_PROPS_add_params.sql

V1.110.0__2021-11-27_DRS20211-1159_alter_column.sql

V1.109.0__2021-11-26_DRS20211-829_alter_column.sql

V1.108.0__2021-11-26_DRS20211-1157-1159-1162_create_table.sql

V1.107.0__2021-11-26_DRS20211-825_dates_and_id_user.sql

V1.106.0__2021-11-26_DRS20211-829_alter_column.sql

V1.105.0__2021-11-24_DRS20211-829_drop_column.sql

V1.104.0__2021-11-24_DRS20211-799_add_code.sql

V1.103.0__2021-11-24_DRS20211-829_create_tables_ekasut_orders.sql

V1.102.0__2021-11-23_DRS2020-800_create_table_ekasut_order_reject_reason.sql

V1.101.0__2021-11-23_DRS20211-799_create_table_ekasut_order_status.sql

V1.100.0__2021-11-23_resize_type_atribute.sql

V1.99.0__2021-11-23_DRS20211-1148_add_ekasut_in_api_systems.sql

R__mdlCore2_getModelTrains.sql

R__prostCalcTdm_getParkId.sql V1.98.0__2021-11-23_DRS2020-490_create_table.sql

V1.97.0__2021-11-19_DRS20211-1183_control_station_esr_nullable.sql

R__armTreeGrafika_dko.sql R__armTreeGrafika_dko.sql

V1.96.0__2021-11-16_DRS20211-1064_dbq_asut_IP_default.sql

V1.95.0__2021-11-16_DRS20211-1128_zak_tra_new_primary_key.sql

V1.94.0__2021-11-16_DRS20211-1082_device_type_fk.sql

V1.93.0__2021-11-15_DRS20211-1108_alter_table_name_comment.sql

V1.92.0__2021-11-15_DRS20211-1057_insert_data_prik_ves_norm.sql

V1.91.0__2021-11-14_DRS20211-1064_dbq_asut_83_88_92_94_96.sql

V1.90.0__2021-11-14_DRS20211-1071_add_mrm_device_color.sql

V1.88.0__2021-11-13_DRS20211-1057_insert_data_prik_ves_ser.sql

V1.87.0__2021-11-13_DRS20211-1057_insert_data_prik_ves_norm.sql

V1.86.0__2021-11-13_DRS20211-1057_alter_column_prik_ves_norm.sql

V1.85.0__2021-11-13_alter_type_traininfo.sql R__pnxArmTreeView_getSostPark.sql

V1.84.0__2021-11-13_DRS20211-1081_insert_data.sql

V1.83.0__2021-11-12_DRS20211-1060_departments_alter_pk.sql

V1.82.0__2021-11-11_DRS20211-1032_add_comment.sql

V1.80.0__2021-11-11_DRS20211-617_id_sequences.sql

V1.79.0__2021-11-10_DRS20211-1032_alter_table.sql

R__pnxArmTreeView_getUchVag.sql

V1.78.0__2021-11-10_resize_column_type.sql

R__wZakrgraf_stInfoJson.sql

R__pkgRepPlanFact_stanSubstList.sql R__pnxArmTreeView_getDepartures.sql

V1.77.0__2021-11-08_add_column_operation_task.sql

R__pnxArmTreeView_getPeregonEsr.sql

V1.76.0__2021-11-08_DRS20211-1001_create_index.sql R__getCoreCar_setIdPoezdFromExtId.sql R__getCoreTrain_processTrainOperation.sql R__getCoreTrain_processTrainOperationGid.sql

V1.75.0__2021-11-08_DRS20211-591_id_sequence.sql

V1.73.0__2021-11-05_DRS20211-968_resize_column.sql

R__wpsSchemeMrm_getMrmPos.sql

R__armTreeGrafika_grafika.sql R__armTreeGrafika_dko.sql

V1.72.0__2021-11-04_DRS20211-972_mng_sys_users_constraints.sql
Уникальность в mng_sys.users по login и e-mail.

R__roleUtils_getTrueFalseGroupRoleForStan.sql

V1.70.0__2021-11-03_DRS20211-931_sys_options_catalog.sql

V1.69.0__2021-11-03_DRS20211-652_add_column_mrm_fn_wagons_reports.sql

V1.68.0__2021-11-03_DRS20211-651_commercial_malfunctions.sql R__nsi_generateCommercialMalfunctions.sql

V1.67.0__2021-11-03_DRS20211-929_alter_type.sql R__pnxArmTreeView_getPodhodList.sql

V1.66.0__2021-11-01_DRS20211-622.sql

R__armTreeBigData_tkpPoly.sql R__pnxArmTreeView_tkpPoly.sql R__wpsSchemeProstPlan_tkpPoly.sql R__wpsShemeProst_tkpPoly.sql

R__pnxArmTreeView_getCarInfo.sql R__pnxLocoworkService_getProstoyBrigOnStan.sql R__pnxLocoworkService_getProstoyLokInDepo.sql R__pnxLocoworkService_getProstoyLokOnStan.sql R__pnxLocoworkService_getStanTotals.sql R__pnxArmTreeView_getProstoySpTdm.sql R__pnxArmTreeView_getProstoyBpTdm.sql R__pnxArmTreeView_getRabParkTdm.sql R__pnxArmTreeView_getVagOborTdm.sql R__wpsAnalysPikopPkg_getOtprTrainPlan.sql R__wpsSchemeProstPlan_getRasform.sql R__wpsSchemeProstPlan_getPeriodProstStanTdm.sql R__wpsSchemeProstPlan_getAllProstStan.sql R__armTreeBigData_byRabParkPodhod.sql R__armTreeBigData_byRabParkPodhodTotals.sql R__armTreeBigData_byRabPark.sql R__armTreeBigData_byRabParkTotals.sql R__armTreeGrafika_npf.sql R__armTreeGrafika_grz.sql R__armTreeBigData_manLok.sql R__armTreeBigData_manLokTotals.sql R__armTreeBigData_byDelivery.sql R__armTreeBigData_byDeliveryTotals.sql R__wpsSchemeMrm_Init.sql R__wpsFuncNavigator_getNaviPath.sql R__pnxArmTreeViewPto_getCrewBusy.sql R__pnxArmTreeViewPto_getZoneWayInfo.sql R__pnxArmTreeViewPto_getZoneInfo.sql R__pnxArmTreeView_getLocInfo.sql R__pnxArmTreeView_getSostPark.sql R__pnxArmTreeViewPto_getParkOccupationPto.sql R__pnxArmTreeViewPto_getParkTotals.sql R__pnxArmTreeViewPto_getCrewPos.sql R__pnxLocoworkService_getParktotalsLok.sql R__pnxLocoworkService_getParkOccupationLok.sql R__pnxLocoworkService_getProstoyLok.sql R__pnxArmTreeView_getParkTotals.sql R__pnxArmTreeView_getParkOccupation.sql R__wpsShemeProst_getProstoyStTr.sql R__pnxArmTreeView_getOccupations.sql R__pnxArmTreeView_getDepartures.sql R__pnxArmTreeView_getNpfList.sql R__wpsStationView_getStationTree.sql R__pnxArmTreeView_initScheme.sql

V1.65.0__2021-10-30_DRS20211-868_add_table_sort_sys_napr.sql

V1.64.0__2021-10-28_DRS20211-801_fill_table_mdl_entity.sql

V1.63.0__2021-10-27_DRS20211-794_asuds_mrm_reports.sql

R__mrmZakr_save8384.sql R__mrmZakr_save8586.sql R__mrmZakr_zakrSave.sql R__wZakrgraf_saveOp8384.sql R__wZakrgraf_saveOp8586.sql R__wZakrgraf_saveOp8586Bash.sql

correct R__wpsJournal_getLocStan.sql R__wpsJournal_getLocStanHand.sql

R__sensorIntegrationData_movingGroup.sql

R__sensorIntegrationData_movingTrains.sql

R__pnxArmTreeView_getPodhodContent.sql

V1.62.0__2021-10-22_DRS20211-640_alter_type.sql

V1.61.0__2021-10-22_fill_tpws.sql

V1.60.0__2021-10-20_fill_table_nsi_prik-ves-norm.sql

V1.59.0__2021-10-20_fill_table_nsi_prik-ves-ser.sql

R__pnxArmTreeViewPto_getParkTotals.sql

correct R__clearDbPoly_clearSkpi.sql

R__mdl_parkPrioritet.sql

V1.58.0__2021-10-18_DRS20211-541_create_table_ds004.sql

V1.57.0__2021-10-12_DRS20211-73_create_table_api_system_station.sql

R__zakr_triggerZakJourPoezdPrUTrg.sql

V1.56.0__2021-10-08_DRS20211-540_add_report_mrm.sql

V1.55.0__2021-10-07_DRS20211-42_nsi.technological_process-alter.sql

V1.54.0__2021-10-07_DRS20211-632_alter_table_mrm_fn_task_reports.sql

V1.53.0__2021-10-06_DRS20211-622_nsi.asoup3_operation_technological_operation-create.sql

V1.52.0__2021-10-06_DRS20211-468_new_table_mrm.sql

V1.51.0__2021-10-04_load_cargo_operations_table.sql

V1.50.0__2021-10-04_fixbug_create_sheme_modeling_settings.sql

V1.49.0__2021-10-04_fixbug_alter_table_mdl_property_values.sql

R__pkgRepPlanFact_getFactNotInEsrNom.sql

V1.48.0__2021-10-01_DRS20211-415_mng_sys.pnx_permissions-insert.sql

V1.47.0__2021-09-30_DRS20211-364_nsi_polygons_add_data.sql

V1.46.0__2021-09-30_DRS20211-196_fix_timer.sql

V1.45.0__2021-09-30_DRS20211-422_clear_nameob_tmo.sql

V1.44.0__2021-09-30_DRS20211-374_nsi.stan_view_geo_drop_not_null_lat_lng.sql Можно сохранять точки в гео зонах без координат.

R__gateAsutWej_processr306.sql R__gateAsutWej_getParamWDel.sql

V1.42.0__2021-09-29_DRS20211-353-399_add_pnx_permissions.sql

R__gateAsutWej_processr306.sql

V1.43.0__2021-09-28_DRS20211-314_altertable_mdl.locomotive_crew_operation-mdl-timestamp.sql

V1.41.0__2021-09-28_DRS20211-298_add_id_obj_to_graf_peregon.sql

V1.40.0__2021-09-28_DRS20211-409_technological_process_data-insert.sql

V1.39.0__2021-09-27_DRS20211-397_dbq_update_DBQ_EXTCALL.sql R__dbqCore_synFindIdOperationFirst.sql

V1.38.0__2021-09-27_DRS20211-352_add_column_ns_kateg_poezd_mdl.sql

V1.37.0__2021-09-24_DRS20211-246_add_aps.sql

V1.36.0__2021-09-23_DRS20211-125_asoup_integration.sql

R__tdm_createViewTmLokBrigPrmt1.sql R__tdm_createViewTmLokBrigPrmt2.sql

V1.35.0__2021-09-20_DRS20211-373_fill_table_mdl_locomotive_uol.sql

V1.34.0__2021-09-20_DRS20211-350_new_table_view_tm_lok_brig_prmt.sql

V1.33.0__2021-09-20_DRS20211-371_add_comment_crew_recreation_time.sql

V1.32.0__2021-09-15_DRS2020-1883_add_new_bad_locomotive_status.sql

V1.31.0__2021-09-09_DRS20211-292_add_new_trip_sign.sql

V1.30.0_2021-09-13-DRS2020-773_insert_table_version2.sql

V1.29.0__2021-09-09-DRS20211-293_asoup-nsi_asoup_dor-alter-size.sql

V1.28.0__2021-09-08-DRS20211-68_wps.json_station_map_with_geo-alter.sql

R__mdlCore2_getmodelcontrolactions.sql

V1.27.0__2021-09-08-DRS2020-773_insert_table.sql

V1.26.0__2021-09-DRS20211-285_soap_history_for_init.sql

V1.25.0__2021-09-06_DRS20211-265_add_unique_fk_constraint.sql

V1.24.0__2021-09-03_DRS2020-1748_add_tables_missing_base_element.sql

V1.23.0__2021-09-01_DRS20211-208_add_column.sql

V1.22.0__2021-09-02_DRS20211-246_fill_nsi_apps.sql

R__pnxArmTreeView_DetOnWayLokLongStand.sql

V1.21.0__2021-09-01_DRS2020-1870_nsi_pikop_nazn_display_add_row.sql

V1.19.0__2021-08-31_DRS20211-147_nsi.technological_process-alter_column_type.sql

V1.18.0__2021-08-31_DRS20211-147_nsi.operation_task-recreate.sql

V1.17.0__2021-08-30_DRS2020-1869_mdl_opcode_matching_drop_reference_and_not_null.sql

V1.16.0__2021-08-31_DRS20211-196_create_timer.sql

V1.15.0__2021-08-30_DRS20211-147_nsi.technological_process_operation-alter_not_nulls.sql

V1.14.0__2021-08-26_DRS20211-192_add_data.sql

V1.13.0__2021-08-26_DRS20211-15_mng_sys.pnx_permissions-update.sql

V1.13.0__2021-08-26_DRS2020-1833_indexes_for_eco_tomsk_queries

Индексы для одного из запросов в API Эко-Томск: выборка сортировочных листов вагононов для получения назначений плана формирования.
V1.12.0__2021-08-25_DRS20211-60_json_station_map_with_geo.sql

V1.11.0__2021-08-25_DRS20211-147_nsi.technological_process-alter_column_type.sql

V1.10.0__2021-08-24_DRS20211-153_alter_column_nsi.gid_stan_subst.sql

V1.9.0__2021-08-23_DRS20211-147_nsi.technological_process_operation-add_column.sql

V1.8.0__2021-08-20_DRS20211-36_add_pk_nsi_stan_view_geo.sql

V1.7.0__2021-08-20_DRS20211-147_nsi.operation-alter_not_nulls.sql

V1.6.0__2021-08-20_DRS20211-48_nsi.technological_process-alter_column_types.sql

V1.5.0__2021-08-19_DRS20211-48_nsi.technological_process_editor_process_view-create.sql

R__dbqCore_getResponseByIdFirstMessage.sql

V1.4.0__2021-08-19_DRS20211-134_app_settings_table.sql

V1.3.100__2021-08-19_DRS20211-160_nsi.technological_process_object-nsi.technological_process_type-insert.sql

V1.3.99__2021-08-19_DRS20211-147_technological_process_new_structure-create.sql

V1.3.98__2021-08-18_DRS20211-151_alter_table.sql

V1.3.97__2021-08-18_DRS20211-151_alter_table.sql

V1.3.96__2021-08-18_DRS20211-171_create_graf_peregon_put.sql

V1.3.95__2021-08-17_DRS20211-151_add_alter_table.sql

V1.3.94__syntetic_off.sql

V1.3.93__2021-08-17_DRS20211-134_create_public_user_settings_table.sql

V1.3.92__2021-08-17_DRS2020-1844_new_column_mrm_fn_task.sql

V1.3.91__2021-08-16_DRS2020-983_nsi_klient_stan.sql

Добавлен признак использования в АРМ СЛОТ.
V1.3.90__2021-08-16_DRS20211-155_nsi.tpws_oper-remove_redundant_operations.sql

V1.3.89__2021-08-16_DRS20211-93_create_table.sql

V1.3.88__2021-08-13_DRS2020-1838_nsi_pikop_nazn_display.sql

V1.3.87__2021-08-13_DRS20211-89_update_mark_map_table.sql

V1.3.86__2021-08-11_DRS20211-89_fix_default_value.sql

V1.3.85__2021-08-06_DRSCORE-168_updating_uol_tables.sql

V1.3.84__2021-08-05_DRS20211-95_insert_mng_sys_pnx_permissions.sql

V1.3.83__2021-08-04_DRS20211-48_nsi.technological_process_editor_process_view-create.sql

V1.3.82__2021-08-03_DRS20211-93_correct_table.sql

V1.3.81__2021-07-30_DRS20211-23_nsi.ns_vid_poezd-create-insert.sql

V1.3.80__2021-08-02_DRS20211-93_add_comment_table.sql

V1.3.79__2021-07-30_DRS20211-21_nsi.tpws_protz-alter.sql

V1.3.78__2021-07-30_DRS20211-45_create_table_mrm-fn-task-reports.sql

V1.3.77__2021-07-30_DRS2021-31_add_row_api_systems.sql

V1.3.76__2021-07-30_correct_table_api_systems.sql

V1.3.75__2021-07-30_DRS20211-15_mng_sys.pnx_permissions-insert.sql

V1.3.74__2021-07-30_DRS20211-93_create_table.sql

V1.3.73__2021-07-30_DRS20211-47_correct_table_mrm.sql

V1.3.72__DRS2020-1777_add_comment_column.sql

V1.3.71__DRS2021-391_add_row_rest_service_routes.sql

R__wpsJournal_getLocPodh.sql R__wpsJournal_getLocStan.sql R__wpsJournal_getLocStanHand.sql

V1.3.70__DRSCORE-60_alter_name.sql

V1.3.69__2021-07-20_tm_plan_op_primary_key.sql

V1.3.68__DRSCORE-1775_alter_tables.sql

V1.3.67__DRSCORE-49_add_cascade_delete.sql

V1.3.66__2021-07-20_plan_loc_primary_key.sql

DRS2020-1794(19.07.2021 Будрик) R__wpsJournal_getLocStan.sql R__wpsJournal_getLocStanHand.sql

V1.3.63__DRS2020-1775_fix.sql

V1.3.64__2021-07-15_DRSCORE-60_add_constraint_unique.sql

V1.3.63__sync_table.sql

V1.3.62__DRS2020-1775.sql

R__wpsJournal_getLocPodh.sql R__wpsJournal_getLocStan.sql

V1.3.61__DRSCORE-49_fix3.sql

R__wpsJournalSmAdv_delPlanFormVar.sql R__wpsJournalSmAdv_offCurPlanFormVar.sql R__wpsJournalSmAdv_putNote.sql R__wpsJournalSmAdv_putPlanFormVar.sql R__wpsJournalSmAdv_putPlanFormVarV2.sql R__wpsJournalSmAdv_putPoezdSheduleV2.sql R__wpsJournalSmAdv_updPlanFormVar.sql

V1.3.60__2021-07-09_DRS2021-1499_gateCore_tryCreateTrain.sql

R__wpsJournal_getLocPodh.sql

V1.3.59__2021-07-05_DRS2021-301_sensors_integration_data_ppss_issue_report_configuration_category-update.sql

V1.3.58__2021-06-08_DRSCORE-28_create_modeling_core_train_oper.sql

V1.3.57__DRSCORE-99_t_sprav_vrab.sql

V1.3.56__DRSCORE-49_fix2_double.sql

V1.3.55__2021-07-01_DRSCORE-60_add_constraint_unique.sql

V1.3.54__2021-00-01_DRS2021-303_sensors_integration_data_ppss_issue_report_configuration_property-insert.sql

R__gateAsutWej_getResponce.sql

R__gateAsutWej_processr303.sql

R__wpsJournal_getNormProbeg.sql

R__mdlCore2_getmodelcarscontrolaction.sql R__mdlCore2_getModelCarsTrain.sql R__mdlCore2_getModelCarsTrainNat.sql R__mdlCore2_getModelCarsTrainNatRoad.sql R__mdlCore2_getModelCarsTrainRoad.sql

V1.3.53__syntetic.sql

R__clearDbPoly_clearHardTdmTabs.sql

V1.3.52__2021-06-18_DRS2021-361_asuds_mrm_fn_ispol-add_not_null.sql

V1.3.51__DRSCORE-49_fix2.sql

V1.3.50__2021-06-17_DRS2021-239_fix_alter_column.sql

R__wpsSchemeProstPlan_getAllProstStan.sql

R__wpsSchemeProstPlan_getPeriodProstStanTdm.sql

V1.3.49__2021-06-16_DRS2021-334_sensors_integration_data.mals_configuration_invisible_object_xxx-create.sql

V1.3.48__2021-06-11_create_index_skpi.sql

V1.3.47__2021-06-11_DRS2021-190_sensors_integration_data_skpi_sensor_group_xxx-new_data.sql

V1.3.46__2021-06-11_create_index_ppss.sql R__clearDbPoly_clearPpss.sql R__clearDbPoly_clearSkpi.sql R__clearDbPoly_runClear.sql

V1.3.45__2021-06-10_DRS2021-190_sensors_integration_data_skpi_sensor_group_xxx-recreate.sql

V1.3.43__2021-06-05_DRS2020-1745_name_station_detailing_correct.sql

README.md

V1.3.42__2021-06-05_DRS2021-275_create_index_ppss.sql

V1.3.41__2021-06-05_DRS2021-310_sensors_integration_data_ppss_issue_report_configuration_xxx-create.sql

V1.3.40__2021-06-05_DRS2020-1745_name_station_detailing.sql

V1.3.39__2021-06-05_DRS2021-298_create_table_skpi_queue.sql

V1.3.38__2021-06-04_DRSCORE-41_dispatcher_thread.sql

V1.3.37__2021-06-04_DRS2021-190_sensors_integration_data_skpi_sensor_group_xxx-recreate.sql

V1.3.36__2021-06-03_correct_comment_column.sql

V1.3.35__2021-06-02_create_table_ppss_image.sql

V1.3.34__2021-06-01_tkp_prost_vag_add_diff_idle.sql

V1.3.33__2021-05-31_add_column_ppss-train-report.sql

V1.3.32__2021-05-31_DRS2020-1738_add_column.sql

V1.3.31__2021-05-31_DRS2021-190_sensors_integration_data_skpi_sensor_group_xxx-create.sql

V1.3.30__2021-05-31_DRS2021-239_add_field.sql

V1.3.29__2021-05-30_DRS2021-243_create_table_ksau_queue.sql

V1.3.28__DRSCORE-49_fix.sql

V1.3.27__2021-05-26_DRSCORE-60_add_new_tables.sql

V1.3.26__DRSCORE-54_api.systems.sql

V1.3.25__2021-05-24_DRSCORE-20_alter_table_bad_trains.sql

V1.3.24__2021-05-25_DRS2020-1028_clearDB.sql

R__getCoreCar_processVagOperation.sql(убран вызов сохранения порядковых номеров)

V1.3.23__2021-05-24_DRSCORE-63_alter_table.sql

V1.3.22__2021-05-24_DRS2021-159_sensors_data_connection_ppss_carriage_issue_report_log_status-alter.sql

V1.3.21__2021-05-24_DRS2021-102_correct_table.sql

V1.3.20__DRSCORE-53_fix.sql

V1.3.19__gid_route2_gid_route_pf2_del_redundant_functions.sql

V1.3.18__DRSCORE-53_cnsi_integration.sql

V1.3.17__2021-05-18_DRS2021-159_sensors_data_connection_ppss_carriage_issue_xxx_create.sql

V1.3.16__DRSCORE-50_fix.sql

R__wpsSchemeCore_init.sql(DRS2020-1712)

R__gateCore_tryGetTrainOldNew.sql(DRS2020-1725)

R__nsi_grafIdsGeneration.sql

V1.3.15__DRSCORE-38_tm_plan_op.sql

R__nsi_grafIdsGeneration.sql

V1.3.14__DRSCORE-49_output_norm.sql

V1.3.13__DRSCORE-49_locomotive_turnaround_section.sql

V1.3.12__2021-05-14_alter_view_v_zak_journal.sql

V1.3.11__DRSCORE-39_modeling_road.sql

V1.3.10__2021-05-13_fix_alter_agreement_employees_columns_type.sql

V1.3.9__DRSCORE-21_mdl_plan_op.sql

V1.3.8__plan_form_marking_map_cit_czs_104.sql

V1.3.7__DRSCORE-21_mdl_plan_op.sql

V1.3.6__plan_form_marking_map_cit_czs_105.sql

V1.3.5__2021-05-11_fix_DRSCORE-26_alter_table_change_type.sql

V1.3.4__2021-05-11_DRSCORE-2_fix_tech_process_step_references_comments.sql

V1.3.3__update_mdl_core2_cars_fix.sql

V1.3.2__update_mdl_core2_cars.sql

V1.3.1__2021-05-07_DRSCORE-8_create_tm_lok_brig_prmt_all_table.sql

V1.3.0__pkgRepPlanFact_trainInfo_add_pn_sost_f.sql R__wpsJournal_sendZakazAsut.sql R__pkgRepPlanFact_getInfoForAsut.sql

V1.2.99__trigger_tm_vag_delete_trg.sql

V1.2.98__2021-05-05_DRSCORE-26_add_mark_map.sql

V1.2.97__2021-05-03_DRS2021-134_create_mals_czs_linking_tables.sql

V1.2.96__TM_VAG_STAN.sql V1.2.95__2021-05-04-DRSCORE-6_new_tables.sql

R__getCore_tryCreateTrain_idPoezd_esrF_pnSost_esrN_vrsvop_prForm_ggSrc.sql

R__getCoreCar_processTmVagNom.sql R__getCoreCar_processVagOperation.sql [DRS2020-1547] - Ведение истории порядковых номеров вагонов(tdm.tm_vag_nom).

V1.2.94__ASOUP_INTEGRATION.sql

V1.2.93__2021-04-30_DRSCORE-2_create_tech_process_tables.sql

Таблицы техпроцессов нового ядра моделирования.

V1.2.92__2021-04-30_DRS2020-1662_add_column_sl_slv.sql

V1.2.91__2021-04-29_DRS2021-153_add_a_normalized_alternative_to_the_reason_types.sql

V1.2.90__PLAN_FORM_MARKING_MAP_CIT_CZS.sql V1.2.89__PLAN_FORM_MARKING_MAP.sql

R__wpsJournalSmAdv_putPoezdSheduleV2.sql

V1.2.88__TM_VAG_BRAK_fix.sql V1.2.87__TM_VAG_OP_fix.sql

R__getCore_tryFindTrainByExt.sql

V1.2.86__TM_VAG_BRAK.sql V1.2.85__TM_VAG_OP_new_columns.sql

Добавлена папка repeatable_scripts на повторяющиеся скрипты(хранимые процедуры) Добавлен новый скрипт R__wpsJournal_getPrmtBrig.sql(функция wps_journal.getPrmtBrig)

V1.2.84__2021-04-21_DRS2021-84_alter_table_add_data.sql

V1.2.83__2021-04-21_DRS2021-145_resize_column_vu23_faults.sql

V1.2.82__2021-04-21_DRS2020-1645.sql

V1.2.81__2021-04-20_DRS2021-140_create_functions_sensor_integration.sql

V1.2.80__2021-04-19_DRS2021-102_create_table_sorting_list.sql

V1.2.79__2021-04-19_DRS2021-139_create_table_nsi_recoding_mark_sorting_list_cit.sql

V1.2.78__2021-04-19_DRS2020-1613_close_old_train.sql

V1.2.77__2021-04-16_DRS2020-1613_clear_depart_train.sql

V1.2.76__2021-04-16_DRS2020-1626_clear_train_not_vag.sql

V1.2.75__2021-04-16_DRS2020-1632_pnx_WaggonList_ObjectHistory.sql Натурный лист и история поезда

pnx_information_service.getPairedObjectsHistory
pnx_sidebar_asuds.getWaggonList
V1.2.74__2021-04-15_DRS2021-11_correct_script.sql

V1.2.73__2021-04-14_DRS2021-130_add_column.sql Добавлены поля в таблицы

sensors_integration_data.ksau_free_area_way
sensors_integration_data.ksau_length_free_area_way
sensors_integration_data.ksau_mnvr
sensors_integration_data.ksau_reshuffle_wagon_group
sensors_integration_data.ksau_rospusk_fact
sensors_integration_data.ksau_rospusk_fact_wagon
sensors_integration_data.ksau_rospusk_oper
sensors_integration_data.ksau_change_otcep_route
sensors_integration_data.ksau_way_numb_route
V1.2.72__2021-04-14_DRS2021-123_create_skpi_sensor_table.sql

V1.2.71__2021-04-14_DRS2021-120_add_column.sql Добавлены поля в таблицы

sensors_integration_data.skpi_train_fencing
sensors_integration_data.skpi_train_fixing
sensors_integration_data.skpi_data_from_stu_dso
sensors_integration_data.skpi_data_from_locomotive
sensors_integration_data.skpi_data_from_stu_dso_list_vagons
sensors_integration_data.skpi_data_from_askin
sensors_integration_data.skpi_data_from_askin_vagon
V1.2.70__2021-0414_DRS2021-119_create_table.sql

Добавлено поле sensors_integration_data.ppss_train_report.tdm_train_id
Добавлена таблица sensors_integration_data.systems
Добавлены пользователи ППСС, КСАУ СП, СКПИ
V1.2.69__2021-04-12_create_mals_table.sql

V1.2.68__2021-04-12_create_foreign_key_skpi_table.sql

V1.2.67__2021-04-07_getcachedroute_fix.sql

Исправлен вызов построения пути следования при отсутствии его в кэше
V1.2.66__2021-04-07_podhod_from_tdm.sql

Получение подхода без схемы tref_076
V1.2.65__2021-04-08_DRS2021-87_delete_unique_index_vu23_fault.sql

V1.2.64__2021-04-06_correct_nsi_type_carriage_repair.sql

V1.2.63__2021-04-02_DRS2021-101_add_ppss_attribute.sql

V1.2.62__2021-04-02_DRS2020-1402_correct_bug_gateCore_insUpdTmVag.sql Исправлена ошибка возврата вагонов на станцию, после прихода операции после отправления.

V1.2.61__2021-04-02_DRS2021-62_fix_bug.sql

V1.2.60__2021-04-02_correct_clear_prost_tsft.sql Отключение чистки простоев на стендах ТСФТ

V1.2.59__2021-04-02_DRS2021-86_fix_bug.sql

V1.2.58__2021-03-31_DRS2020-1599_default_UOL_rows.sql

файл миграции данных справочного типа - приказы УОЛ по локомотивам
V1.2.57__2021-03-31_DRS2021-84_alter_tables.sql

V1.2.56__2021-03-31_DRS2020-1565_add_function.sql

V1.2.55__2021-03-31_create_table_vu23.sql

V1.2.54__2021-03-31_DRS2021-7_create_MALS_geo_plan_aspect_tables.sql

V1.2.53__2021-03-30_DRS2021-84_add_data.sql

V1.2.52__2021-03-29_DRS2021-84_add_new_tables.sql

add spring-boot-starter-actuator V1.2.50__2021-03-26_correct_function_GateCore_TryOpenTrain.sql

V1.2.49__2021-03-24_DRS2021-6_19_add_MALS_OBJECT_and_MALS_ENGINE_with_data.sql

V1.2.48__DRS2021-73_create_table_train_carriage_operation.sql

V1.2.47__2021-03-23_DRS2020-1583_add_tm_plan_op_sequence.sql

V1.2.46__2021-03-22_DRS2020-1577_add_column_NsiShemeProstPark.sql

V1.2.45__2021-03-22_DRS2020-1576_correct_wpsShemeProst_getFaktProstTdm.sql

V1.2.44__2021-03-22_DRS2021_33_alter_column.sql

V1.2.43__2021-03-22_DRS2021_60_update_data.sql

V1.2.42__2021-03-19_DRS2021_62_update_data.sql

V1.2.41__2021-03-19_DRS2021_59_add_columnl.sql

V1.2.40__2021-03-19_DRS2021_55_add_table_alarm_level.sql

V1.2.39__2021-03-19_DRS2021-60_create_ppss_catalog.sql

V1.2.38__2021-03-18_DRS2021_create_table_pps_nsi_fix_bug.sql

V1.2.37__2021-03-18_DRS2021-12_alter_ksau_reshuffle_wagon_group.sql

V1.2.36__2021-03-17_DRS2021-54_alter_ksau_table.sql

V1.2.35__2021-03-17_DRS2021-17_alter_ksau_change_otcep_route.sql

V1.2.34__2021-03-16_update_ns015.sql

V1.2.33__2021-03-16_DRS2020-1566_gateСore_trygettrain.sql

V1.2.32__2021-03-16_DRS2020-1565_pnxArmTreeView_initScheme.sql

V1.2.31__2021-03-16_DRS2020-1564_pnxArmTreeView_DetIdleCars.sql

V1.2.30__2021-03-16_DRS2021_create_table_pps_nsi.sql

V1.2.29__2021-03-16_DRS2021-46_create_ksau_rospusk_fact.sql

V1.2.28__2021-03-16_DRS2020-1373_comment_column.sql

V1.2.27__2021-03-15_DRS2020-1402_correct_function.sql

V1.2.26__2021-03-15_DRS2021-56_create_table_PpssControlPoint.sql

V1.2.25__2021-03-15_DRS2021-37_add_columns_for_history.sql

V1.2.24__2021-03-15_DRS2021-37_create_api_schema_tables_and_savedate_columns.sql

V1.2.23__2021-03-15_DRS2021-19_SENSORS_INTEGRATION_DATA.MALS_GEO_PLAN_xxx_create.sql

V1.2.22__2021-03-15_DRS2021-52_create_table_ diff_time_operation.sql

V1.2.21__2021-03-12_DRS2021-33_create_tables_ppss_types.sql

V1.2.20__2021-03-12_DRS2021-14_alter_ksau_mnvr_ksau_rospusk_oper.sql

V1.2.19__2021-03-12_tranzitOst_only_CIT.sql

V1.2.18__2021-03-12_DRS2020-1553_mapping_train_tst.sql

V1.2.17__2021-03-12_DRS2020-1373_update_table_dictionary_container_group_name.sql

V1.2.16__2021-03-11_DRS2021-12_create_ksau_reshuffle_wagon_group_table.sql

V1.2.15__2021-03-11_DRS2020-1552_osNagr_tonna.sql

V1.2.14__2021-03-11_DRS2020-1530_correct_insert_op_train_gid_05.sql

V1.2.13__2021-03-10_DRS2021-16_create_ksau_free_area_way_table.sql

V1.2.12__2021-03-10_DRS2020-1373_alter_table_dictionary.sql

V1.2.11__2021-03-10_DRS2020-1542_nsi_get_priznak_def.sql

V1.2.10__2021-03-10_DRS2021-3_SENSORS_INTEGRATION_DATA.PPSS_xxx_create.sql

V1.2.9__2021-03-09_DRS2021-16_create_ksau_change_otcep_route_table.sql

V1.2.8__2021-03-09_DRS2021-11_correct_script_skpi.sql

V1.2.7__2021-03-09_DRS2020-1536_clear_mdl_vagon_group.sql

V1.2.6__2021-03-09_DRS2020-1533_clear_zakr_zakUhod.sql

V1.2.5__2021-03-09_DRS2020-1402_GateFix.sql

V1.2.4__2021-03-09_DRS2020-1537_GateCore_TryGetTrain.sql

V1.2.3__2021-03-09_W_ZAKRGRAF_3_29_14.sql

V1.2.2__2021-03-09_DRS2020-1501_clear_db.sql

V1.2.1__2021-03-05_DRS2020-1522_tranzit_ost.sql

V1.1.99__2021-03-05_DRS2020-1520_correct_getData.sql

V1.1.98__2021-03-09_DRS2020-1285_insert_eco_tomsk_user.sql

V1.1.97__2021-03-05_DRS2021-15_create_log_table.sql

V1.1.96__2021-03-05_DRS2020-1508_insert_nsi_ZakUtsNorm.sql

V1.1.95__2021-03-05_DRS2020-1507_for_TKP_correct_scripts.sql

V1.1.94__2021-03-05_DRS2021-10_correct_table_skpi.sql

V1.1.93__2021-03-03_DRS2020-1428_alter_table.sql

V1.1.92__2021-03-03_DRS2020-1375_update_table.sql

V1.1.91_2021-03-03DRS2020-1513_for_TKP_PRD_ISD.sql

V1.1.90_2021-03-03DRS2020-1512_for_TKP_getCurrentTrainLoco_needChangeLengthWeight.sql

V1.1.89_2021-03-03DRS2020-1507_for_TKP.sql

V1.1.88__2021-03-03_DRS2020-1398_create_table_crew_recreation_time.sql

V1.1.87__2021-03-03_DRS2020-1213_create_mileage_norm_table_and_insert_default.sql

V1.1.86__2021-03-03_DRS2020-1437_SENSORS_INTEGRATION_DATA.MALS_xxx_create.sql

V1.1.85__2021-03-03_DRS2020-657-NEURO_STRUCTURE.xxx_create.sql ...

V1.1.77__2021-03-01_DRS2020-1490_Clear_vag.sql

V1.1.76__2021-03-01_DRS2020-1487_insert_zak_uts_norm.sql

V1.1.75__2021-03-01_MRM_ZAKR_3_18.sql

V1.1.74__2021-03-01_ZAKR_TREE_VIEW_2_14_5.sql

V1.1.73__2021-03-01_W_ZAKRGRAF_3_29_6.sql

V1.1.72__2021-03-01_ZAKRGRAF.sql

V1.1.71__2021-02-25_DRS2020-1471_NRP_TM-VAG-OP.sql

V1.1.70__2021-02-25_load_planning_bug_fix.sql

V1.1.69__2021-02-24_DRS2020-1424_kod_sost_locomotive.sql

V1.1.68__2021-02-24_addoponstan_and_getmodeltrains.sql

V1.1.67__2021-02-24_DRS2020-1457_update_mdl_core2_getmodelcarstrainroad.sql

V1.1.66__2021-02-24_DRS2020-565_set_fixed_id_to_cargo_techprocesses.sql

V1.1.65__2021-02-24_DRS2020-1428_create_schema_eco.sql

V1.1.64__2021-02-20_DRS2020-1417_MDL_LocomotiveUol.sql

V1.1.63__2021-02-20_DRS2020-1426.sql

V1.1.62__2021-02-20_Prost_Calc.sql

V1.1.61__2021-02-20_NsiCore_GetTmoParent.sql

V1.1.60__2021-02-19_DRS2020-1430_gateFix_setlocfix.sql

V1.1.59__2021-02-19_DRS2020-1446_pkgrepplanfact_getloktrfactinfo.sql

V1.1.58__2021-02-19_DRS2020-1261_CreateTable_section_job_locomotive_crew.sql

V1.1.57__2021-02-19_DRS2020-1411_DRS2020-1432_GateFix.sql

V1.1.56__2021-02-18-DRS2020-1427_PROST_CALC.sql

V1.1.55__2021-02-15-DRS2020-558-fixes_for_cargo_modelling.sql (Яромир 15.02.2021)

V1.1.54__2021-02-15-DRS2020-1402-gate_core_car.sql

V1.1.51 - V1.1.53 Белокопытов А. DRS2020-1373

V1.1.50__2021-02-09-DRS2020-1249_TEST_FlyWay.sql

Тестирование сервиса
V1.1.48__2021-02-08-NSI.TWPS_xxx-postgres-alter.sql (Горьков С. 09.02.2021)

V1.1.47__2021-02-05-DRS2020-1358_WpsJournal.getFreeLokBrig.sql

V1.1.46__2021-02-05-DRS2020-1256_GateCoreCar.ProcessVagOperation.sql

V1.1.45__2021-02-04-WPS_JOURNAL_CORRECT.sql

V1.1.44__2021-02-04-ADD_VIEW_DBQ.V_MESSAGE_RESPONSE.sql

V1.1.43__2021-02-02-DRS2020-1342_GATE_CORE_TRAIN.GetTranzitOst.sql

V1.1.42__2021-02-01-DRS2020-1335_GetFreeLokBrig.sql

V1.1.41__2021-02-01-NSI_TN_PF_MARSH_USER_PG.sql

V1.1.40__2021-02-01-OTHERS_1-38_1-39.sql

V1.1.39__2021-02-01-GID_ROUTE_PF2.sql

V1.1.38__2021-02-01-GID_ROUTE2.sql

V1.1.37__2021-01-28-WPS_JOURNAL_SendZakazAsut.sql

V1.1.36__2021-01-28-PKGREPPLANFACT_GetInfoForAsut.sql

V1.1.1 - V1.1.35 - 01.01.2021 kirdub

Взято из postgreSQL_repo
V1.0.132, V1.0.133 - 01.01.2021 kirdub

Перенесено из старой версии db-migration-service
V1.0.131 - 25.01.2021 kirdub

Первоначальная миграция БД.